<{{'?php'}}

namespace App\{{$sourcePath}}\Cards;

use MaxinTeam\Adele\Cards\Card;

class {{$className}} extends Card
{

    protected function fields() {
        return [
            // fields here
        ];
    }

}