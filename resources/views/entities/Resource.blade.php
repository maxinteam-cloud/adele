<{{'?php'}}

namespace App\{{$sourcePath}}\Resources;

use Illuminate\Http\Request;

use MaxinTeam\Adele\Resources\Resource;
use MaxinTeam\Adele\Resources\Fields\ID;

use {{$modelUse}};
@if ($cardUse)
use {{$cardUse}};
@endif

class {{$className}} extends Resource
{

    protected $model = {{$modelName}}::class;
    @if ($cardName)
    protected $card = {{$cardName}}::class;
    @endif

    public function boot(Request $request, $admin) {
        // init resource method
    }

    protected function fields() {
        return [
            ID::make(),
            // fields here
        ];
    }

    protected function filters() {
        return [
            // filter fields here
        ]; 
    }

}