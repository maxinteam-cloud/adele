<{{'?php'}}

namespace App\Adele\Modifiers;

use MaxinTeam\Adele\Modifiers\Modifier;

class {{$className}} extends Modifier
{

    public function modify($value) {
        return $value;
    }

}