<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$__adele->title}}</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
    <link href="/vendor/adele/css/vendor.css" rel="stylesheet">
    <link href="/vendor/adele/css/adele.css" rel="stylesheet">

    <script src ="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/v-mask/dist/v-mask.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue2-datepicker@3.10.4/index.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/vue2-datepicker@3.10.4/index.css" rel="stylesheet">
</head>
<body>
    <div id = "main-wrapper" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
        <header id = "header" class="topbar" data-navbarbg="skin6">
                    <nav class="navbar top-navbar navbar-expand-md navbar-light">
                        <div class="navbar-header" data-logobg="skin5">
                            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
                                <i class="ti-menu ti-close"></i>
                            </a>
                            <div class="navbar-brand">
                                <a href="#" class="logo">
                                    <b class="logo-icon">
                                        <img src="https://hacktheme.github.io/Nice-Admin/assets/images/logo-light-icon.png" alt="homepage" class="light-logo">
                                    </b>

                                    <span class="logo-text">
                                        <img src="https://hacktheme.github.io/Nice-Admin/assets/images/logo-light-text.png" class="light-logo" alt="homepage">
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin6">
                            <ul class="navbar-nav float-start me-auto">
                                <!-- <li class="nav-item search-box">
                                    <a class="nav-link waves-effect waves-dark" href="javascript:void(0)">
                                        <div class="d-flex align-items-center">
                                            <i class="fas fa-search"></i>
                                            <div class="ms-1 d-none d-sm-block">
                                                <span>Search</span>
                                            </div>
                                        </div>
                                    </a>
                                    <form class="app-search position-absolute" style="display: none;">
                                        <input type="text" class="form-control" placeholder="Search &amp; enter">
                                        <a class="srh-btn">
                                            <i class="ti-close"></i>
                                        </a>
                                    </form>
                                </li> -->
                            </ul>
                            <ul class="navbar-nav float-end">
                                <li class="nav-item dropdown">
                                    <a @@click.prevent = "openMenu" class="nav-link dropdown-toggle waves-effect waves-dark pro-pic" href="#" role="button" aria-expanded="false" :class = "{show: openedMenu}">
                                        <!-- <img src="https://www.wrappixel.com/demos/free-admin-templates/nice-admin-lite/assets/images/users/2.jpg" alt="user" class="rounded-circle" width="31"> -->
                                        <i class="fas fa-user-secret"></i>
                                        <span>{{$__adele->admin->login}}</span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-end user-dd animated" :class = "{show: openedMenu}">
                                        <a class="dropdown-item" href="{{$__adele->getPathRouteByAlias('logout')}}"><i class="fas fa-sign-out-alt"></i> Выйти</a>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
            </header>

            <aside class="left-sidebar" data-sidebarbg="skin5">
            <div class="scroll-sidebar">
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="in">
                        <!-- <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin" aria-expanded="false">
                                <i class="fas fa-tachometer-alt"></i>
                                <span class="hide-menu">Статистика</span>
                            </a>
                        </li> -->
                        @foreach ($__adele->nav as $item)
                        @if ($item->type == 'delimiter')
                            <hr/>
                            @if ($item->label)
                            <div class = "delimiterHeader">{{$item->label}}</div>
                            @endif
                        @else
                        <li class="sidebar-item {{trim(\Request::path(), '/') == trim($item->path, '/') ? 'selected' : ''}}">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/{{trim($item->path, '/')}}" aria-expanded="false">
                                <i class="fas fa-{{$item->icon}}"></i>
                                <span class="hide-menu">{{$item->label}}</span>
                            </a>
                        </li>
                        @endif
                        @endforeach
                    </ul>
                </nav>
            </div>
        </aside>
    
        <div class="page-wrapper" style="display: block;">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">{{$__adele->title}}</h4>
                    </div>
                    <!-- <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="/admin">Административная панель</a>
                                    </li>
                                    @if (false)
                                        @foreach ($breadcrumbs as $url => $item)
                                        <li class="breadcrumb-item">
                                            <a href="/{{$url}}">{{$item}}</a>
                                        </li>
                                        @endforeach
                                    @endif
                                    <li class="breadcrumb-item active" aria-current="page">{{isset($breadcrumbsCurrent) ? $breadcrumbsCurrent : ''}}</li>
                                </ol>
                            </nav>
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="container-fluid">
                @yield('content')
            </div>
            <footer class="footer text-center">
                Adele - Laravel Admin Panel
            </footer>
        </div>
    
    
    </div>

    <script>
        Vue.use(VueMask.VueMaskPlugin);
        Vue.use(DatePicker);
        
        Vue.component('custom-select', {
            props: ['name', 'items', 'value', 'searchable'],
            data: () => ({
                opened: false,
                openTop: false,
                selected: {
                    value: null,
                    label: 'Выберите',
                },
                options: [],
                search: false,
                q: ''
            }),
            created() {
                const onClickOutside = e => {
                    if (!this.$el.contains(e.target)) {
                        this.close();
                    }
                }
                document.addEventListener('click', onClickOutside);
                // this.$on('hook:beforeDestroy', () => document.removeEventListener('click', onClickOutside));
            },
            mounted() {
                this.options = JSON.parse(this.items);
                if (this.options && typeof Object.values(this.options)[0] != 'object') {
                    for (let index in this.options) {
                        this.options[index] = {
                            value: this.options[index],
                            label: this.options[index]
                        };
                    }
                }
                if (this.value) {
                    let elem = this.options.find(el => el.value == this.value);
                    this.selected = elem ? elem : this.selected;
                }
                if (this.searchable !== undefined) {
                    this.search = true;
                }
            },
            watch: {
                value() {
                    let elem = this.options.find(el => el.value == this.value);
                    this.selected = elem ? elem : this.selected;
                }
            },
            template: `
                <div class = "form-control customSelect">
                    <div @@click = "open" class = "customSelect__value">
                        <span v-show = "!search || !opened">@{{this.selected.label}}</span>
                        <input v-show = "search" type = "text" ref="search" v-model = "q" />
                    </div>
                    <div class = "customSelect__options" :class = "{opened, 'customSelect__options--top': openTop}">
                        <div v-for = "item in filteredOptions" :key = "item.value" @@click.prevent = "selectItem(item)" class = "customSelect__option" :class = "{'customSelect__option--selected': item.value == selected.value}">
                            @{{item.label}}
                        </div>
                        <div class = "customSelect__optionsEmpty" v-show = "filteredOptions.length == 0">Нет результатов</div>
                    </div>
                    <input type = "hidden" :name = "name" :value = "selected.value" />
                </div>
            `,
            computed: {
                filteredOptions() {
                    return this.options.filter(option => !this.q || option.label.toLowerCase().indexOf(this.q.toLowerCase()) != -1);
                }
            },
            methods: {
                toggleOpenTop() {
                    let bottomPos = this.$el.getBoundingClientRect().bottom + document.documentElement.scrollTop;
                    let optionsElemHeight = this.$el.querySelector('.customSelect__options').offsetHeight;
                    this.openTop = document.body.clientHeight - optionsElemHeight - bottomPos < 0;
                },
                open(e) {
                    this.opened = true;
                    this.toggleOpenTop();
                    if (this.search) {
                        this.$refs.search.focus();
                    }
                },
                close() {
                    this.q = '';
                    this.opened = false;
                },
                selectItem(item) {
                    this.selected = item;
                    this.close();
                    this.$emit('input', item.value);
                }
            }
                                
        });
        Vue.component('custom-multi-select', {
            props: ['name', 'items', 'value'],
            data: () => ({
                opened: false,
                openTop: false,
                selected: [],
                options: [],
                q: ''
            }),
            created() {
                const onClickOutside = e => {
                    if (!this.$el.contains(e.target)) {
                        this.close();
                    }
                }
                document.addEventListener('click', onClickOutside);
                
                // this.$on('hook:beforeDestroy', () => document.removeEventListener('click', onClickOutside));
            },
            mounted() {
                this.options = JSON.parse(this.items);
                if (this.value) {
                    this.selected = JSON.parse(this.value);
                    this.selected = this.selected.map(el => el + '');
                }                    
            },
            template: `
                <div class = "form-control customSelect customSelect--multi">
                    <div @@click = "open" class = "customSelect__value">
                        <div v-for = "item in selectedOptions" :key = "item.value" class = "customSelect__valueItem">
                            <span>@{{item.label}}</span>
                            <i class="fas fa-times-circle" @@click.prevent = "deleteItem(item.value)"></i>
                        </div>
                        <input type = "text" ref="search" v-model = "q" />
                    </div>
                    <div class = "customSelect__options" :class = "{opened, 'customSelect__options--top': openTop}">
                        <div v-for = "item in filteredOptions" :key = "item.value" @@click.prevent = "selectItem(item.value)" class = "customSelect__option">
                            @{{item.label}}
                        </div>
                        <div class = "customSelect__optionsEmpty" v-show = "filteredOptions.length == 0">Нет результатов</div>
                    </div>
                    <input type = "hidden" :name = "name + '['+ item +']'" v-for = "item in selected" :key = "item" :value = "item" />
                </div>
            `,
            computed: {
                selectedOptions() {
                    return this.options.filter(option => this.selected.includes(option.value + ''));
                },
                filteredOptions() {
                    return this.options.filter(option => !this.selected.includes(option.value + '') && (!this.q || option.label.toLowerCase().indexOf(this.q.toLowerCase()) != -1));
                },
            },
            methods: {
                toggleOpenTop() {
                    let bottomPos = this.$el.getBoundingClientRect().bottom + document.documentElement.scrollTop;
                    let optionsElemHeight = this.$el.querySelector('.customSelect__options').offsetHeight;
                    this.openTop = document.body.clientHeight - optionsElemHeight - bottomPos < 0;
                },
                open(e) {
                    this.toggleOpenTop();
                    this.opened = true;
                    this.$refs.search.focus();
                },
                close() {
                    this.q = '';
                    this.opened = false;
                },
                selectItem(value) {
                    if (this.selected.includes(value + '')) {
                        this.selected = this.selected.filter(el => el != value);
                    }
                    else {
                        this.selected.push(value + '');
                    }
                    this.close();
                },
                deleteItem(value) {
                    this.selected = this.selected.filter(el => el != value);
                }
            }
                                
        });

        function __createApp(costruct, id) {
            let app = new Vue({el: '#' + id, ...costruct});
        }


        const Header = {
            created() {
                const onClickOutside = e => {
                    if (!this.$el.contains(e.target)) {
                        this.openedMenu = false;
                    }
                }
                document.addEventListener('click', onClickOutside);
            },
            data: () => ({
                openedMenu: false
            }),
            methods: {
                openMenu() {
                    this.openedMenu = true;
                }
            }
        };

        __createApp(Header, 'header');

    </script>

    @yield('scripts')
</body>
</html>