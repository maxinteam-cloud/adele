@extends('adele::layout')

@section('content')
<div class = "card" id = "form">
    <div class = "card-body">
        <div v-show = "successMessage" class = "alert alert-success">Изменения успешно сохранены<br><a href = "{{url()->previous()}}">Назад в листинг</a></div>
        <form class = "form-horizontal" @@submit.prevent = "submit">
            @foreach ($fields as $field)
                @if ($field->type == 'select')
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    @php 
                        $items = [];
                        foreach ($field->options as $optionValue => $optionLabel) {
                            $items[] = [
                                'value' => $optionValue,
                                'label' => $optionLabel
                            ];
                        }
                        $value = '';
                    @endphp
                    <{{$field->multi ? 'custom-multi-select' : 'custom-select'}} name = "{{$field->column}}" items = "{{json_encode($items)}}" value = "{{$value}}" />

                </div>
                @elseif ($field->type == 'boolean')
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    @php 
                        $items = [
                            [
                                'value' => 1,
                                'label' => 'Да',
                            ],
                            [
                                'value' => 0,
                                'label' => 'Нет',
                            ]
                        ];
                        $value = '';
                    @endphp
                    <custom-select name = "{{$field->column}}" items = "{{json_encode($items)}}" value = "{{$value}}" />
                </div>
                @elseif ($field->type == 'text')
                <div class="form-group">
                    <label>{{$field->label}}</label>
                    @if ($field->big)
                    <textarea class="form-control" name = "{{$field->column}}">{{$field->value}}</textarea>
                    @else
                    <input type="{{$field->inputType}}" class="form-control" name = "{{$field->column}}" value="{{$field->value}}">
                    @endif
                </div>
                @endif
            @endforeach
            <button type = "submit" class = "mt-2 btn btn-primary">{{$editMode ? 'Сохранить изменения' : 'Создать'}}</button>
            @if (false && $editMode)
            <a href = "../" class = "mt-2 btn btn-secondary">Отменить</a>
            @endif
        </form>
    </div>
</div>

<style>
    .image > img {
        margin-bottom: 20px;
        max-width: 300px;
        max-height: 100px;
    }
</style>
@endsection

@section('scripts')
<script>
        const Form = {
            data() {
                return {
                    successMessage: false,
                    mode: '{{$editMode ? 'edit' : 'create'}}'
                }   
            },
            computed: {
            },
            methods: {
                async submit(event) {
                    let data = new FormData(event.target);
                    const response = await axios.post(window.location.origin + window.location.pathname, data);
                    if (response.data.success) {
                        if (this.mode == 'edit') {
                            this.successMessage = true;
                        }
                        else {
                            window.location.href = '{{url()->previous()}}';
                        }
                    }
                    else {
                        this.successMessage = false;
                    }
                }
            }
        }

        __createApp(Form, 'form');

    </script>
@endsection