@extends('adele::layout')

@section('content')
<div class = "card" id = "grid">
    <div class = "card-body">
        <div v-show = "deleted" class = "alert alert-danger">Запись успешно удалена</div>
        <div class = "filter">
            <div class = "row align-items-start">
                <div class = "col-10">
                    <form class = "filter-form" @@submit.prevent = "filterAction">
                    @if (count($filters))
                        <div class = "row">
                            @foreach ($filters as $filterField)
                            <div class = "col-4">
                                @if ($filterField->type == 'text')
                                <div class="form-group">
                                    <label>{{$filterField->label}}</label>
                                    @if ($filterField->mask) 
                                    <input type="text" v-mask = "filterMask.{{$filterField->column}}.mask" v-model = "filterMask.{{$filterField->column}}.value" :placeholder = "placeholderMask(filterMask.{{$filterField->column}}.mask)" class="form-control">
                                    <input type = "hidden" name = "{{$filterField->column}}" :value = "filterMask.{{$filterField->column}}.value">
                                    @else
                                    <input type="text" class="form-control" name = "{{$filterField->column}}" value = "{{$filterValues[$filterField->column] ?? ''}}" />
                                    @endif
                                </div>
                                @elseif ($filterField->type == 'select')
                                <div class="form-group">
                                    <label>{{$filterField->label}}</label>
                                    @php 
                                        $items = !$filterField->multi ? [['value' => '', 'label' => $filterField->labelAll]] : [];    
                                        foreach ($filterField->options as $itemValue => $itemLabel) {
                                            $items[] = [
                                                'value' => $itemValue,
                                                'label' => $itemLabel
                                            ];
                                        }
                                        $value = isset($filterValues[$filterField->column]) ? ($filterField->multi ? json_encode(array_values($filterValues[$filterField->column])) : $filterValues[$filterField->column]) : null;
                                    @endphp
                                    <{{$filterField->multi ? 'custom-multi-select' : 'custom-select'}} name = "{{$filterField->column}}" items = "{{json_encode($items)}}" {{$value ? "value = $value" : ''}}  {{$filterField->searchable ? 'searchable' : ''}}/>
                                </div>
                                @elseif ($filterField->type == 'boolean')
                                <div class="form-group">
                                    <label>{{$filterField->label}}</label>
                                    @php 
                                        $items = [
                                            [
                                                'value' => -1,
                                                'label' => 'Все'
                                            ],
                                            [
                                                'value' => 1,
                                                'label' => 'Да',
                                            ],
                                            [
                                                'value' => 0,
                                                'label' => 'Нет',
                                            ]
                                        ];
                                        $value = isset($filterValues[$filterField->column]) ? $filterValues[$filterField->column] : '';
                                    @endphp
                                    <custom-select name = "{{$filterField->column}}" items = "{{json_encode($items)}}" value = "{{$value}}" />
                                </div>
                                @elseif ($filterField->type == 'number')
                                <div class="form-group">
                                    <label>{{$filterField->label}}</label>
                                    @if (!$filterField->range)
                                    <input type="number" min = "{{$filterField->min}}" max = "{{$filterField->max}}" step = "{{$filterField->step}}" class="form-control" name = "{{$filterField->column}}" value = "{{isset($filterValues[$filterField->column]) ? $filterValues[$filterField->column] : ''}}">
                                    @else
                                    <div class="row">
                                        <div class = "col-6">
                                            <input type="number" min = "{{$filterField->min}}" max = "{{$filterField->max}}" step = "{{$filterField->step}}" placeholder = "От" name = "{{$filterField->column}}[from]" value = "{{isset($filterValues[$filterField->column]['from']) ? $filterValues[$filterField->column]['from'] : ''}}" class="form-control">
                                        </div>
                                        <div class = "col-6">
                                            <input type="number" min = "{{$filterField->min}}" max = "{{$filterField->max}}" step = "{{$filterField->step}}" placeholder = "До" name = "{{$filterField->column}}[to]" value = "{{isset($filterValues[$filterField->column]['to']) ? $filterValues[$filterField->column]['to'] : ''}}" class="form-control">
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                @elseif ($filterField->type == 'date')
                                <div class="form-group">
                                    <label>{{$filterField->label}}</label>
                                    <date-picker v-model = "filterDate.{{$filterField->column}}.value" lang="ru" type="{{$filterField->dateType}}" format="{{$filterField->dateType == 'date' ? 'DD.MM.YYYY' : 'DD.MM.YYYY HH:mm:ss'}}" {{$filterField->range ? 'range' : ''}} :input-attr = "{name: '{{$filterField->column}}'}" />
                                </div>
                                @endif
                            </div>
                            @endforeach
                            <div class = "col-12">
                                <div class="form-group">
                                    <button class = "btn btn-primary">Применить</button>&nbsp;
                                    <button type = "button" class = "btn btn-secondary" @@click.prevent = "reload">Сбросить</button>
                                </div>
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
                <div class = "col-2 createBtnWrap">
                @if ($createLink)
                    <a href = "{{$createLink}}" class = "btn btn-primary"><i class="fas fa-plus"></i> Создать</a>
                @endif
                </div>
            </div>
        </div>
        <div class = "result" :class = "{loading}">
            <div class = "row mb-3">
                    <div class = "col-10" v-show = "countPages > 1">
                        <nav aria-label="pagination">
                            <ul class="pagination">
                                <li class="page-item">
                                <a class="page-link" href="#" @@click.prevent = "selectPage(currentPage - 1)" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                                </li>
                                <li class="page-item" v-for = "page in pages" :class = "{'active': Number(page) == currentPage}" :key = "page">
                                    <a class="page-link" href="#" @@click.prevent = "selectPage(page)" v-if = "page != -1 && page != 0">@{{page}}</a>
                                    <a class="page-link" v-else>...</a>
                                </li>
                                <li class="page-item">
                                <a class="page-link" href="#" @@click.prevent = "selectPage(currentPage + 1)" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                                </li>
                            </ul>
    
                        </nav>
                    </div>
                    <div class = "col-2" :class = "{'hide-pp': loading}">
                        <div class = "text-muted">Показывать по</div>
                        <custom-select items = "{{json_encode($__adele->resourceSettings['perPage']['options'])}}" v-model = "perPage" />
                    </div>
                </div>
            <table v-show = "items.length" class = "table table-bordered table-hover items">
                <caption>Показаны @{{startPos}}-@{{endPos}} из @{{count}} записей</caption>
                <thead class = "table-light">
                    <tr>
                        @foreach ($fields as $field)
                        <th data-column = "{{$field->sortColumn}}" style = "width: {{$field->width != 0 ? $field->width.'%' : 'auto'}};" {{$field->sortable ? '@@click.prevent = sortColumn(\''.$field->sortColumn.'\') class = sort-column' : ''}} :class = "{ sortable: sort.column == '{{$field->sortColumn}}' }">
                            <span>{{$field->label}}</span>
                            @if($field->sortable)
                            <i class="fas" :class = "{'fa-sort': sort.column != '{{$field->sortColumn}}', 'fa-caret-up': sort.column == '{{$field->sortColumn}}' && sort.by == 'asc', 'fa-caret-down': sort.column == '{{$field->sortColumn}}' && sort.by == 'desc'}"></i>
                            @endif
                            </th>
                        @endforeach
                        <th style = "width: 1%" v-show = "isActions">Действия</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for = "item in items" :key = item.id>
                        <td v-for = "field in item.fields" :style = "{width: field.width != 0 ? field.width + '%' : 'auto', 'white-space': field.oneline ? 'nowrap' : 'inherit'}">
                            <template v-if = "field.type == 'image' && field.multi">
                                <img v-for = "(img, index) of field.value" :key = "index" class = "image" :src = "img" alt = "Изображение не найдено" />
                            </template>
                            <img class = "image" v-else-if = "field.type == 'image'" :src = "field.value" alt = "Изображение не найдено" />
                            <span v-else v-html = "field.value" />
                        </td>
                        <td style = "width: 1%" class = "actions" v-show = "isActions">
                            <span class = "actions__group">
                                <a v-for = "action in item.actions" :key = "action.name" @@click.prevent = "actionItem(action, item.id)" class = "actions__item">
                                    <i class="fas" :class = "'fa-' + action.icon" :title = "action.label"></i>
                                </a>
                            </span>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div v-show = "!first && items.length == 0" class="alert alert-danger" role="alert">
                Ничего не найдено
            </div>
            <div class = "row">
                <div class = "col-10" v-show = "countPages > 1">
                    <nav aria-label="pagination">
                        <ul class="pagination">
                            <li class="page-item">
                            <a class="page-link" href="#" @@click.prevent = "selectPage(currentPage - 1)" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                            </li>
                            <li class="page-item" v-for = "page in pages" :class = "{'active': Number(page) == currentPage}" :key = "page">
                                <a class="page-link" href="#" @@click.prevent = "selectPage(page)" v-if = "page != -1 && page != 0">@{{page}}</a>
                                <a class="page-link" v-else>...</a>
                            </li>
                            <li class="page-item">
                            <a class="page-link" href="#" @@click.prevent = "selectPage(currentPage + 1)" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                            </li>
                        </ul>

                    </nav>
                </div>
                <div class = "col-2" :class = "{'hide-pp': loading}">
                    <div class = "text-muted">Показывать по</div>
                    <custom-select items = "{{json_encode($__adele->resourceSettings['perPage']['options'])}}" v-model = "perPage" not-searcheble />
                </div>
            </div>
            <div class="spinner-border loader" v-show = "loading" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>
        </div>
    </div>
</div>

<style>
    .items th {
        vertical-align: baseline;
    }

    .filter {
        margin-bottom: 20px;
    }

    .createBtnWrap {
        display: flex;
        justify-content: flex-end;
    }

    .actions {
        white-space: nowrap;
    }

    .actions__item {
        font-size: 1.125em;
        opacity: .7;
        cursor: pointer;
        color: inherit;
        transition: all .3s;
    }

    .actions__item:hover {
        color: #7460ee;
        opacity: 1;
    }

    .actions__item + .actions__item {
        margin-left: 10px;
    }

    .actions__group + .actions__group {
        margin-left: 10px;
        padding-left: 10px;
        border-left: 1px solid #ccc;
    }

    .action-show {
        transform: translateY(1px);
    }

    .image {
        max-height: 50px;
        max-width: 200px;
        margin-bottom: 10px;
        margin-right: 10px;
    }
    
    .sort-column i {
        margin-left: 7px;
    }

    .sort-column {
        cursor: pointer;
        white-space: nowrap;
    }

    .sort-column.sortable {
        background: #233242;
        color: white;
    }

    .sort-column.sortable i {
        font-size: 1.25em;
        transform: translateY(2px);
    }

    .result {
        position: relative;
    }

    .result.loading {
        min-height: 200px;
    }

    .result.loading::after {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: rgba(255,255,255,.8);
        z-index: 9;
    }

    .result.loading .loader {
        position: absolute;
        top: 50%;
        left: 50%;
        width: 3rem;
        height: 3rem;
        margin-left: -1.5rem;
        margin-top: -1.5rem;
        z-index: 10;
    }

    .hide-pp {
        visibility: hidden;
    }

</style>
@endsection

@section('scripts')
<script>
        const Grid = {
            data() {
                return {
                    first: true,
                    deleted: false,
                    currentUrlPath: window.location.origin + window.location.pathname,
                    currentUrl: window.location.origin + window.location.pathname + '/',
                    count: 0,
                    items: [],

                    filterMask: {
                        @foreach ($filters as $filterField)
                        @if ($filterField->type == 'text' && $filterField->mask)
                        {{$filterField->column}}: {
                            mask: '{{$filterField->mask}}',
                            value: '{{isset($filterValues[$filterField->column]) ? $filterValues[$filterField->column] : ''}}'
                        },
                        @endif
                        @endforeach
                    },
                    filterDate: {
                        @foreach ($filters as $filterField)
                        @if ($filterField->type == 'date')
                        '{{$filterField->column}}': {
                            value: '{{isset($filterValues[$filterField->column]) ? $filterValues[$filterField->column] : ''}}',
                            inputAttrs: {name: '{{$filterField->column}}'}
                        },
                        @endif
                        @endforeach
                    },
                    
                    currentPage: {{$page}},
                    showCountPages: 15,
                    sort: {
                        column: {!!$sort ? "'{$sort['column']}'" : 'null'!!},
                        by: {!!$sort ? "'{$sort['by']}'" : 'null'!!}
                    },
                    loading: false,
                    perPage: {{$perPage}}
                }
            },
            computed: {
                countPages() {
                    return Math.ceil(this.count / this.perPage);
                },
                startPos() {
                    return (this.currentPage - 1) * this.perPage + 1;
                },
                endPos() {
                    const end = this.currentPage * this.perPage;
                    return end <= this.count ? end : this.count;
                },

                startPage() {
                    const middlePosition = this.currentPage - Math.floor(this.showCountPages / 2);
                    return middlePosition > 0 ? middlePosition : 1;
                },
                finishPage() {
                    const lastPosition = this.startPage + this.showCountPages - 1;
                    return lastPosition < this.countPages ? lastPosition : this.countPages;
                },
                pages() {
                    let res = [];
                    if (this.startPage > 1) {
                        res.push(1);
                    }
                    if (this.startPage > 2) {
                        res.push(-1);
                    }
                    for (let i = this.startPage; i <= this.finishPage; i++) {
                        res.push(i);
                    }
                    if (this.finishPage < this.countPages - 1) {
                        res.push(0);
                    }
                    if (this.finishPage < this.countPages) {
                        res.push(this.countPages);
                    }
                    return res;
                },
                isActions() {
                    for (let item of this.items) {
                        if (item.actions && item.actions.length) {
                            return true;
                        }
                    }

                    return false;
                }
            },
            watch: {
                async perPage(value) {
                    this.currentPage = 1;
                    await this.getItems();
                }
            },
            methods: {
                getFilter() {
                    const form = document.querySelector(".filter-form");
                    const fd = new FormData(form);
                    let filter = {};
                    fd.forEach((value, key) => filter[key] = value);
                    return this.parseInputs(filter);
                },
                async getItems(setUrl = true) {
                    let params = {page: this.currentPage, perPage: this.perPage, filter: this.getFilter(), sort: this.sort};
                    this.loading = true;
                    const response = await axios.post(this.currentUrlPath, params);
                    window.scrollTo(0, 0);
                    this.items = response.data.items;
                    this.count = response.data.count;
                    this.loading = false;
                    this.first = false;

                    if (setUrl) {
                        let queryString = this.serializeQueryString(params);
                        history.pushState(null, null, encodeURI(this.currentUrl + '?' + queryString));
                    }

                },
                async filterAction() {
                    this.currentPage = 1;
                    await this.getItems();
                },
                async selectPage(page) {
                    if (page > 0 && page <= this.countPages && page != this.currentPage) {
                        this.currentPage = page;
                        await this.getItems();
                    }
                },
                async sortColumn(column) {
                    if (this.sort.column == column) {
                        if (this.sort.by == 'asc') {
                            this.sort.by = 'desc';
                        }
                        else {
                            this.sort = {
                                column: null,
                                by: null
                            };
                        }
                    }
                    else {
                        this.sort = {
                            column,
                            by: 'asc'
                        };
                    }

                    await this.getItems();
                },
                async deleteItem(action, id) {
                    if (confirm('Вы уверены, что хотите удалить запись?')) {
                        const response = await axios.delete('/' + action.path);
                        if (response.status == 200) {
                            this.items = this.items.filter(item => item.id != id);
                            this.deleted = true;
                        }
                    }
                },
                actionItem(action, id) {
                    if (action.name == 'delete') {
                        this.deleteItem(action, id);
                    }
                    else if (action.isPage) {
                        window.location.href = '/' + action.path;
                    }
                },
                reload() {
                    window.location.href = this.currentUrlPath;
                },

                parseInputs(data) {
                var ret = {};
                retloop:
                    for (var input in data) {
                        var val = data[input];

                        var parts = input.split('[');       
                        var last = ret;

                        for (var i in parts) {
                            var part = parts[i];
                            if (part.substr(-1) == ']') {
                                part = part.substr(0, part.length - 1);
                            }

                            if (i == parts.length - 1) {
                                last[part] = val;
                                continue retloop;
                            } else if (!last.hasOwnProperty(part)) {
                                last[part] = {};
                            }
                            last = last[part];
                        }
                    }
                    return ret;
                },
                placeholderMask(mask) {
                    return mask.replaceAll('#', '_');
                },
                serializeQueryString(params, prefix) {
                    let str = [],
                        p;
                    for (p in params) {
                        if (params.hasOwnProperty(p)) {
                        var k = prefix ? prefix + "[" + p + "]" : p,
                            v = params[p];
                        if (v !== null && v != '') {
                            str.push((v !== null && typeof v === "object") ?
                                this.serializeQueryString(v, k) :
                                k + "=" + v);
                            }
                        }
                    }
                    return str.filter(el => el.trim() != '').join("&");
                }
            },
            mounted() {
                this.$nextTick(() => {
                    this.getItems(false);
                });
            },
        }

        __createApp(Grid, 'grid');

    </script>
@endsection