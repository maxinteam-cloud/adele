<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Adele - вход</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" rel="stylesheet">
    <link href="/vendor/adele/css/vendor.css" rel="stylesheet">
    <link href="/vendor/adele/css/adele.css" rel="stylesheet">

    <style>

        .page-wrapper {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
        }

        .page-wrapper > .card {
            width: 300px;
        }

    </style>

    <script src ="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
    <script src="https://unpkg.com/vue@next"></script>
</head>
<body>
    <div class = "page-wrapper" id = "login-form">
        <div class = "card">
            <div class = "card-body">
                <h3>Авторизация</h3>
                <div v-show = "error" class = "mt-3 alert alert-danger">Неверный логин или пароль</div>
                <form class = "mt-4" @@submit.prevent = "submit">
                    <div class="form-group">
                        <label>Логин</label>
                        <input type="text" class="form-control" v-model = "login">
                    </div> 
                    <div class="form-group">
                        <label>Пароль</label>
                        <input type="password" class="form-control" v-model = "password">
                    </div>   
                    <button type = "submit" class = "mt-2 btn btn-primary">Авторизоваться</button>
                </form>
            </div>
        </div>
    </div>

    <script>
        const Form = {
            data() {
                return {
                    login: '',
                    password: '',
                    error: false,
                }   
            },
            methods: {
                async submit(event) {
                    this.error = false;
                    const response = await axios.post(window.location, {login: this.login, password: this.password});
                    if (response.data.result == 'success') {
                        window.location.href = response.data.redirect;
                    }
                    else {
                        this.error = true;
                    }
                }
            }
        }

        Vue.createApp(Form).mount('#login-form');

    </script>
</body>