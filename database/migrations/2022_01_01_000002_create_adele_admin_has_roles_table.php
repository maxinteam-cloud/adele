<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdeleAdminHasRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adele_admin_has_roles', function (Blueprint $table) {
            $table->foreignId('adele_admin_id')->constrained()->onDelete('cascade');
            $table->foreignId('adele_admin_role_id')->constrained()->onDelete('cascade');

            $table->primary(['adele_admin_id', 'adele_admin_role_id']);
            $table->index('adele_admin_id');
            $table->index('adele_admin_role_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adele_admin_has_roles');
    }
}
