<?php

namespace MaxinTeam\Adele\Models;

use Illuminate\Database\Eloquent\Model;

class AdeleAdminRole extends Model
{
    const SUPERUSER_CODE = 'su';

    public $timestamps = false;
    protected $guarded = [];

}
