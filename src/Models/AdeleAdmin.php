<?php

namespace MaxinTeam\Adele\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Arr;

class AdeleAdmin extends Model
{
    protected $guarded = [];

    public function roles() {
        return $this->belongsToMany(AdeleAdminRole::class, 'adele_admin_has_roles');
    }

    public function hasRole(string|array $roles) : bool {
        return $this->roles->pluck('code')->diff(Arr::wrap($roles))->count() < $this->roles->count();
    }

    public function hasRoles(array $roles) : bool {
        return $this->roles->pluck('code')->diff($roles)->count() == $this->roles->count() - count($roles);
    }

    public function isSuperUser() : bool {
        return $this->hasRole(AdeleAdminRole::SUPERUSER_CODE);
    }

    public function getPublicDataAttribute() {
        $roles = $this->roles;
        return [
            'login' => $this->login,
            'name' => $this->name,
            'roles' => [
                'codes' => $roles->pluck('code'),
                'labels' => $roles->pluck('name')
            ]
        ];
    }
}
