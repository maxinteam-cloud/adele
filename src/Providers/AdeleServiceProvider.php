<?php

namespace MaxinTeam\Adele\Providers;

use Illuminate\Support\ServiceProvider;

use MaxinTeam\Adele\Adele;
use MaxinTeam\Adele\AdeleView;
use MaxinTeam\Adele\AdeleViewData;
use MaxinTeam\Adele\AdeleAuthGuard;

use MaxinTeam\Adele\Console\Commands;

use MaxinTeam\Adele\Router\Router;
use MaxinTeam\Adele\Menu\Menu;
use MaxinTeam\Adele\Menu\MenuItem;
use MaxinTeam\Adele\Menu\MenuGroup;

use MaxinTeam\Adele\Http\Middleware\AdeleAuth;
use MaxinTeam\Adele\Http\Middleware\AdeleAccessRoute;
use MaxinTeam\Adele\Http\Middleware\AdeleSetViewData;
use MaxinTeam\Adele\__Admin\Resources;

use Illuminate\Session\Middleware\StartSession;

class AdeleServiceProvider extends ServiceProvider
{

    public function boot()
    {

        $this->publishes([
            $this->packagePath('config/adele.php') => config_path('adele.php'),
            $this->packagePath('public') => public_path('vendor/adele'),
        ], 'adele');

        $this->loadMigrationsFrom($this->packagePath('database/migrations'));

        if ($this->app->runningInConsole()) {
            $this->commands([
                Commands\AdeleSuperUserCommand::class,
                Commands\AdeleResourceMakeCommand::class,
                Commands\AdeleModifierMakeCommand::class,
                Commands\AdeleCardMakeCommand::class,
            ]);
        }

        $this->loadViewsFrom($this->packagePath('resources/views'), 'adele');
        
        $this->app->router->middlewareGroup('adele', [StartSession::class, AdeleAuth::class, AdeleAccessRoute::class, AdeleSetViewData::class]);
        
        $this->registerSingletons();

        app(Adele::class)->getRouter()->registerMainRoutes();
        app(Adele::class)->getRouter()->registerAuthRoutes();

        Adele::menu([
            MenuGroup::system()->items([
                MenuItem::delimiter('Администраторы'),
                MenuItem::resource(Resources\__AdeleAdminRoleResource::class, 'Роли администраторов', 'user-tag'),
                MenuItem::resource(Resources\__AdeleAdminResource::class, 'Администраторы', 'user-secret'),
            ]),
        ]);
    }

    private function registerSingletons() {
        $singletons = [Router::class, Menu::class, AdeleAuthGuard::class, Adele::class];
        foreach ($singletons as $singleton) {
            $this->app->singleton($singleton);
        }
    }

    private function packagePath(string $path) : string {
        return __DIR__.'/../../' . $path;
    }

}