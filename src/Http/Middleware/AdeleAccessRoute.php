<?php

namespace MaxinTeam\Adele\Http\Middleware;

use Closure;

use MaxinTeam\Adele\Router\Router;

class AdeleAccessRoute
{

    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->route()) {
            $route = Router::getRouteOrFail($request->route()->getName());
            if (!$route->isAccess()) {
                abort(403);
            }
        }

        return $next($request);
    }
}
