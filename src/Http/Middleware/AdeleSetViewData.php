<?php

namespace MaxinTeam\Adele\Http\Middleware;

use Closure;

use MaxinTeam\Adele\Adele;

class AdeleSetViewData
{

    public function handle($request, Closure $next, $guard = null)
    {
        $menuItems = app(Adele::class)->getMenu()->getItemsForView();
        $admin = Adele::user();
        app(Adele::class)->getViewData()->setNav($menuItems);
        app(Adele::class)->getViewData()->setUser($admin);

        app(Adele::class)->getViewData()->shareGlobal();

        return $next($request);
    }
}
