<?php

namespace MaxinTeam\Adele\Http\Middleware;

use Closure;
use MaxinTeam\Adele\Adele;
use MaxinTeam\Adele\AdeleAuthGuard;

class AdeleAuth
{

    public function handle($request, Closure $next, $guard = null)
    {
        $authUser = AdeleAuthGuard::auth();

        if (!$authUser) {
            AdeleAuthGuard::setRedirectUrlAfterAuth($request->fullUrl());
            return redirect()->route(app(Adele::class)->getRouter()->genNameRoute('login'));
        }

        return $next($request);
    }
}
