<?php 

namespace MaxinTeam\Adele\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use MaxinTeam\Adele\Adele;

class Controller extends BaseController
{

    public function __construct() {
        //
    }

    private function getPublicData() {
        return app(Adele::class)->getViewData()->getPublicData();
    }

    // public function view($template, $data = []) {
    //     return view("adele::$template", [
    //         'adele' => $this->getPublicData() + $data,
    //     ]);
    // }

    public function home(Request $request) {
        return app(Adele::class)->getView()->view('layout', [
            'title' => 'Главная'
        ]);
    }

}