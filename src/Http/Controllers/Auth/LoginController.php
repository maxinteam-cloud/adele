<?php 

namespace MaxinTeam\Adele\Http\Controllers\Auth;

use Illuminate\Http\Request;

use MaxinTeam\Adele\AdeleAuthGuard;
use MaxinTeam\Adele\Router\Router;

use MaxinTeam\Adele\Adele;

class LoginController extends AuthController
{

    public function __construct(Request $request) {
        //
    }

    public function index(Request $request) {
        return app(Adele::class)->getView()->view('login');
    }

    public function check(Request $request) {
        $result = AdeleAuthGuard::login($request->input('login'), $request->input('password'));
        if ($result !== false) {
            $redirectUri = AdeleAuthGuard::pullRedirectUrlAfterAuth();
            $response = [
                'result' => 'success',
                'redirect' => $redirectUri ? $redirectUri : route(Router::genNameRoute('home'))
            ];
            return json_encode($response);
        }
        return json_encode(['result' => 'error']);
    }

    public function logout(Request $request) {
        AdeleAuthGuard::logout();
        return redirect()->route(Router::genNameRoute('login'));
    }

}