<?php

namespace MaxinTeam\Adele\Http\Controllers;

use MaxinTeam\Adele\Adele;
use MaxinTeam\Adele\Router\Router;
use MaxinTeam\Adele\Cards\Card;
use MaxinTeam\Adele\Resources\Actions\Action;

use Illuminate\Http\Request;

final class CardController extends Controller
{

    private Card $card;
    private Action $action;
    private string $title = '';

    public function __construct() {
        parent::__construct();
        $request = request();
        if ($request->route()) {
            $route = Router::getRouteOrFail($request->route()->getName());
            $this->action = $route->getAction();
            $this->card = $this->action->getCard();
            // $this->title = $this->card->name ? $this->card->name : $route->name;
        }
    }

    public function create(Request $request) {
        return app(Adele::class)->getView()->view('card', [
            'title' => $this->title,
            'fields' => $this->card->getAccesableFields(),
            'editMode' => $this->card->isModeEdit(),
        ]);
    }

    public function edit(Request $request, $id) {
        $this->card->setOriginalObjectById($id);
        return $this->create($request);
    }

    public function store(Request $request) {
        $data = $request->post();
        $validateErrors = $this->card->validate($data);
        if ($validateErrors) {
            return $this->responseError($validateErrors);
        }
        $this->card->fillObject($data);
        $this->card->save();
        $this->responseSuccess();

        return $this->responseSuccess();
    }

    public function update(Request $request, $id) {
        $this->card->setOriginalObjectById($id);
        return $this->store($request);
    }

    private function response(bool $status, array $addedData = []) {
        return response()->json(array_merge([
            'success' => $status,
        ], $addedData)); 
    }

    private function responseError(array $errors = []) {
        return $this->response(false, compact($errors));
    }

    private function responseSuccess() {
        return $this->response(true); 
    }

}