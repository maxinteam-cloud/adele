<?php

namespace MaxinTeam\Adele\Http\Controllers;

use MaxinTeam\Adele\Adele;
use MaxinTeam\Adele\Router\Router;
use MaxinTeam\Adele\Resources\Resource;

use Illuminate\Http\Request;

final class ResourceController extends Controller
{

    private Resource $resource;
    private string $title = '';

    public function __construct() {
        parent::__construct();
        $request = request();
        if ($request->route()) {
            $route = Router::getRouteOrFail($request->route()->getName());
            $this->resource = $route->getResource();
            $this->title = $this->resource->name ? $this->resource->name : $route->name;
        }
    }

    public function index(Request $request) {
        $filterValues = $request->get('filter', []);
        $filterValues = array_merge($filterValues, $this->resource->filterDefaults);
        $sortRequest = $request->get('sort');

        $sort = null;
        if ($sortRequest && isset($sortRequest['column']) && isset($sortRequest['by'])) {
            $sort = $sortRequest;
        }
        else if ($this->resource->sortDefaultColumn && $this->resource->sortDefaultOrder) {
            $sort = [
                'column' => $this->resource->sortDefaultColumn,
                'by' => $this->resource->sortDefaultOrder,
            ];
        }

        $resource = $this->resource;

        app(Adele::class)->getViewData()->resourceSettings['perPage'] = [
            'options' => $resource->perPageOptions ? $resource->perPageOptions : config('adele.resources.perPage.options'),
            'default' => $resource->perPageDefault ? $resource->perPageDefault : config('adele.resources.perPage.default'),
        ];

        return app(Adele::class)->getView()->view('resource', [
            'title' => $this->title,
            'fields' => $resource->getFields(),
            'filterValues' => $filterValues,
            'sort' => $sort,
            'filters' => $resource->getFilters(),
            'perPage' => $request->get('perPage', app(Adele::class)->getViewData()->resourceSettings['perPage']['default']),
            'page' => $request->get('page', 1),
            'createLink' => $resource->getCreateLink(),
        ]);
    }

    public function items(Request $request) {
        $resource = $this->resource;

        $filter = $request->get('filter', []);
        $query = $resource->getQuery($filter);
        
        $count = (clone $query)->count();

        $sort = $request->get('sort');
        if ($sort && $sort['column'] && in_array($sort['by'], ['asc', 'desc'])) {
            $query->orderBy($sort['column'], $sort['by']);
        }

        $page = $request->get('page') ?? 1;
        $perPage = $request->get('perPage') ?? config('adele.resources.perPage.default'); 
        $result = $query->offset(($page - 1) * $perPage)->limit($perPage)->get();

        $items = [];
        foreach ($result as $object) {
            $itemFields = [];
            $itemActions = [];
            foreach ($resource->getFields() as $fieldItem) {
                $field = clone($fieldItem);
                $field->setValueFromObject($object);
                $itemFields[] = $field;
            }
            foreach ($resource->getActions() as $actionItem) {
                $action = clone($actionItem);
                if ($action->forRecord() && $action->isAccessObject($object)) {
                    $itemActions[] = $action->get($object->id);
                }
            }
            $items[] = [
                'id' => $object->id,
                'fields' => $itemFields,
                'actions' => $itemActions,
            ];
        }

        return response()->json([
            'items' => $items,
            'count' => $count
        ]);
    }

    public function delete($id) {
        $resource = $this->resource;
        $objectModel = $resource->getModel();
        $entity = $objectModel::findOrFail($id);
        $entity->delete();

        return response('', 200);
    }

}