<?php 

namespace MaxinTeam\Adele\__Admin\Cards;

use MaxinTeam\Adele\Cards\Card;
use MaxinTeam\Adele\Cards\Fields\Text;

class __AdeleAdminRoleCard extends Card
{

    protected function fields() {
        return [
            Text::make('name', 'Название'),
            Text::make('code', 'Код'),
        ];
    }

}