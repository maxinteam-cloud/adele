<?php

namespace MaxinTeam\Adele\__Admin\Resources;

use MaxinTeam\Adele\Resources\Resource;
use MaxinTeam\Adele\Models\AdeleAdmin;
use MaxinTeam\Adele\Models\AdeleAdminRole;

use MaxinTeam\Adele\Resources\Fields\{ ID, Text, Date };
use MaxinTeam\Adele\Resources\Filters\{ FilterText, FilterSelect };

class __AdeleAdminResource extends Resource
{

    protected $model = AdeleAdmin::class;
    public $path = '__admins';

    protected function boot($request, $admin) {
        //
    }

    protected function fields() {
        return [
            ID::make(),
            Text::make('login', 'Логин')->sortable(),
            Text::make('name', 'Имя')->sortable(),
            Text::make('roles.name', 'Роли'),
            Date::make('created_at', 'Дата создания')->datetime(),
        ];
    }
    
    protected function filters() {
        return [
            FilterText::make('login', 'Логин')->like(),
            FilterText::make('name', 'Имя')->like(),
            FilterSelect::make('roles.id', 'Роль')->fromModel(AdeleAdminRole::class, 'name')->searchable(),
        ]; 
    }
}