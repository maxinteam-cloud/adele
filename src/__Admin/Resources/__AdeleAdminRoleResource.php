<?php 

namespace MaxinTeam\Adele\__Admin\Resources;

use MaxinTeam\Adele\Resources\Resource;
use MaxinTeam\Adele\Models\AdeleAdminRole;

use MaxinTeam\Adele\__Admin\Cards\__AdeleAdminRoleCard;

use MaxinTeam\Adele\Resources\Fields\{ ID, Text };
use MaxinTeam\Adele\Resources\Filters\FilterText;

class __AdeleAdminRoleResource extends Resource
{

    protected $model = AdeleAdminRole::class;
    protected $card = __AdeleAdminRoleCard::class;
    public $path = '__admin_roles';

    protected function boot($request, $admin) {
        $this->accessAction('delete', function($admin, $object) {
            return $object->code != AdeleAdminRole::SUPERUSER_CODE;
        });
        $this->accessAction('edit', function($admin, $object) {
            return $object->code != AdeleAdminRole::SUPERUSER_CODE;
        });
    }

    protected function fields() {
        return [
            ID::make(),
            Text::make('name', 'Название')->sortable(),
            Text::make('code', 'Код'),
        ];
    }
    protected function filters() {
        return [
            FilterText::make('name', 'Название')->like(),
            FilterText::make('code', 'Код')->like()
        ]; 
    }

}