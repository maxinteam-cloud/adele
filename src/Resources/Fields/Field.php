<?php 

namespace MaxinTeam\Adele\Resources\Fields;

use Illuminate\Support\Str;
use MaxinTeam\Adele\Helpers\AdeleAccessable;
use MaxinTeam\Adele\Modifiers\Modifier;

abstract class Field
{

    use AdeleAccessable;

    public string $type = 'field';
    public string $label = '';
    public mixed $column = null;
    public bool $sortable = false;
    public string $sortColumn = '';
    public mixed $value = null;
    public int $width = 0;
    public bool $nowrap = false;
    public mixed $modifier = null;

    protected function __construct() {
        $this->initAccessor();
    }

    public static function make(callable|string $column, string $label = '') {
        $field = new static;
        $field->column = $column;
        $field->label = $label ?? (is_string($column) ? $column : '');
        return $field;
    }

    // set options

    final public function sortable(string $sortColumn = '') : self {
        $this->sortable = true;
        $this->sortColumn = $sortColumn ? $sortColumn : $this->column;
        return $this;
    }

    final public function width($width) : self {
        $this->width = $width;
        return $this;
    }

    final public function nowrap() : self {
        $this->nowrap = true;
        return $this;
    }

    final public function modifier($modifier) : self {
        $this->modifier = $modifier;
        return $this;
    }

    // prepare field

    public function boot() {}

    final public function get() {
        $this->boot();

        if ($this->type == 'field') {
            $this->type = Str::camel(class_basename($this));
        }

        return $this;
    }

    final public function getPipelineColumn() {
        $rels = explode('.', $this->column);
        return $rels;
    }

    final protected function getPipelinePropFromObject($pipe, $object) {
        $prop = array_shift($pipe);
        if (count($pipe)) {
            $relObject = $object->$prop;
            $value = $this->getPipelinePropFromObject($pipe, $relObject);
        }
        else {
            if (is_subclass_of($object, 'Illuminate\Database\Eloquent\Model')) {
                $value = $object->$prop;
            }
            else {
                $value = $object->pluck($prop)->join(', ');
            }
        }

        return $value;
    }

    final public function setValueFromObject($object) {
        if (is_string($this->column)) {
            $column = $this->getPipelineColumn();
            $value = $this->getPipelinePropFromObject($column, $object);
        }
        else {
            $value = ($this->column)($object);
        }

        $value = $this->modifyValue($value, $object);
        if ($this->modifier) {
            if (is_subclass_of($this->modifier, Modifier::class)) {
                $modifierClass = $this->modifier;
                $modifier = new $modifierClass($object);
                $value = $modifier->modify($value);
            }
            else if (is_callable($this->modifier)) {
                $value = ($this->modifier)($value, $object);
            }
        }

        $this->value = $value;
    }

    protected function modifyValue($value, $object = null) { return $value; }

    public static function config($prop, $defaultValue = null) {
        return config('adele.resources.fields.' . $prop, $defaultValue);
    }

}