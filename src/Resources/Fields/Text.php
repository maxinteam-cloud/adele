<?php 

namespace MaxinTeam\Adele\Resources\Fields;

class Text extends Field
{

    private $_prefix = '';
    private $_postfix = '';
    private $withLinks = false;

    public function withLinks() {
        $this->withLinks = true;
        return $this;
    }

    public function prefix($prefix) {
        $this->_prefix = $prefix;
        return $this;
    }

    public function postfix($postfix) {
        $this->_postfix = $postfix;
        return $this;
    }

    public function modifyValue($value, $object = null) {
        $prepareValue = $value;
        if (is_array($value)) {
            $prepareValue = [];
            foreach ($value as $k => $v) {
                $prepareValue[] = $k.' - '.$v;
            }
            $prepareValue = implode("<br>", $prepareValue);
        }
        $prepareValue = ($this->_prefix ? $this->_prefix : '').$prepareValue.($this->_postfix ? $this->_postfix : '');
        if ($this->withLinks) {
            $prepareValue = preg_replace('"\b(https?://\S+)"', '<a href="$1" target = "_blank">$1</a>', $prepareValue);
        }
        return $prepareValue;
    }

}