<?php

namespace MaxinTeam\Adele\Resources\Fields;

use Illuminate\Support\Str;

class Date extends Field
{
    
    public $format = '';
    public string $dateType = 'date'; 

    public function format(string $format) {
        $this->format = $format;
        return $this;
    }

    public function time() {
        $this->dateType = 'time';
        return $this;
    }

    public function datetime() {
        $this->dateType = 'datetime';
        return $this;
    }

    public function boot() {
        if (!$this->format) {
            $this->format = self::config('date.format.' . $this->dateType, 'Y-m-d');
        }
    } 

    public function modifyValue($value, $object = null) {
        return \Carbon\Carbon::create($value)->format($this->format);
    }

}