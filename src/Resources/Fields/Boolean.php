<?php 

namespace MaxinTeam\Adele\Resources\Fields;

class Boolean extends Field
{

    private $trueValue = null;
    private $falseValue = null;

    public function trueValue($value) {
        $this->trueValue = $value;
        return $this;
    }
    
    public function falseValue($value) {
        $this->falseValue = $value;
        return $this;
    }

    public function boot() {
        if (!$this->trueValue) {
            $this->trueValue = self::config('boolean.true');
        }
        if (!$this->falseValue) {
            $this->falseValue = self::config('boolean.false');
        }
    }

    public function modifyValue($value, $object = null) {
        return $value ? $this->trueValue : $this->falseValue;
    }

}