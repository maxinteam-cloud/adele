<?php 

namespace MaxinTeam\Adele\Resources\Fields;

class ID extends Field
{
    
    public static function make($label = null, $column = null) {
        return parent::make($column ?? self::config('id.column'), $label ?? self::config('id.label'));
    }

    public function boot() {
        $this->type = 'id';
        if ($this->width == 0) {
            $this->width = 1;
        }
        $this->sortable();
    }

}