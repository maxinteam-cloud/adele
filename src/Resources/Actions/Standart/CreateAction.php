<?php 

namespace MaxinTeam\Adele\Resources\Actions\Standart;

use MaxinTeam\Adele\Cards\Card;

final class CreateAction extends StandartAction
{

    public function __construct($path, Card $card) {
        parent::__construct($path . '/create', 'plus', 'Создать', 'create');
        $card = clone($card);
        $card->setModeCreate();
        $this->card($card);
        $this->forRecord = false;
    }

}