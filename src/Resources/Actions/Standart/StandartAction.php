<?php 

namespace MaxinTeam\Adele\Resources\Actions\Standart;

use MaxinTeam\Adele\Resources\Actions\Action;

abstract class StandartAction extends Action
{

    public bool $isStandart = true;

}