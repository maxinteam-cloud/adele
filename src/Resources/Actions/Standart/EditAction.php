<?php 

namespace MaxinTeam\Adele\Resources\Actions\Standart;

use MaxinTeam\Adele\Cards\Card;

final class EditAction extends StandartAction
{

    public function __construct($path,  Card $card) {
        parent::__construct($path . '/{id}', 'edit', 'Редактировать запись', 'edit');
        $card->setModeEdit();
        $this->card($card);
    }

}