<?php 

namespace MaxinTeam\Adele\Resources\Actions\Standart;

use MaxinTeam\Adele\Http\Controllers\ResourceController;

final class DeleteAction extends StandartAction
{

    public function __construct($path) {
        parent::__construct($path . '/{id}', 'trash', 'Удалить запись', 'delete');
        $this->handler('delete', [ResourceController::class, 'delete']);
    }

}