<?php 

namespace MaxinTeam\Adele\Resources\Actions;

use MaxinTeam\Adele\Helpers\AdeleAccessable;

use MaxinTeam\Adele\Adele;
use MaxinTeam\Adele\Resources\Resource;
use MaxinTeam\Adele\Cards\Card;

abstract class Action
{

    use AdeleAccessable;

    public string $path = '';
    public string $icon = '';
    public string $label = '';
    public string $name = '';
    public bool $isPage = false;
    public bool $isNewTab = false;
    public bool $isStandart = false;
    protected bool $forRecord = true;
    protected array $handlers = [];
    protected Card|null $card = null;
    protected Resource $resource;
    protected array $cardParams = [];

    public function __construct(string $path, string $icon, string $label = '', string $name = '') {
        $this->path = $path;
        $this->icon = $icon;
        $this->label = $label;
        $this->name = $name;

        $this->initAccessor();
    }

    final protected function handler(string $method, array $action) : void {
        if (is_null($this->card)) {
            if ($method == 'get') {
                $this->isPage = true;
            }
            $this->handlers[$method] = $action;
        }
    }

    final public function card(Card $card, array $params = []) : static {
        $this->card = $card;
        $this->card->setParams($params);
        $this->handlers = $this->card->getHandlers();
        $this->isPage = true;
        return $this;
    }

    final public function newTab() : static {
        $this->isNewTab = true;
        return $this;
    }

    final public function getHandlers() : array {
        return $this->handlers;
    }

    final public function isAccessObject($object) : bool {
        $cb = $this->accessor()->getCallback();
        if ($cb) {
            return $cb(Adele::user(), $object);
        }

        return $this->isAccess();
    }

    final public function forRecord() : bool {
        return $this->forRecord;
    }

    final public function getCard() : Card|null {
        return $this->card;
    }

    final public function setResource(Resource $resource) : void {
        $this->resource = $resource;
    }

    final public function getResource() : Resource {
        return $this->resource;
    }

    final public function get($id) : static {
        $this->path = str_replace('{id}', $id, $this->path);
        return $this;
    }

}