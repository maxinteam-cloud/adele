<?php 

namespace MaxinTeam\Adele\Resources\Filters;

use Illuminate\Support\Str;

class FilterBoolean extends Filter
{

    protected function checkValue($value) {
        return in_array($value, [0, 1]);
    }

}