<?php 

namespace MaxinTeam\Adele\Resources\Filters;

use Illuminate\Support\Str;
use Carbon\Carbon;

class FilterDate extends Filter
{

    const SEPARATOR = ' ~ ';

    public $range = false;
    public $dateType = 'date';

    public function range() {
        $this->range = true;
        return $this;
    }

    public function datetime() {
        $this->dateType = 'datetime';
        return $this;
    }

    protected function checkValue($value) {
        return $this->range ? count(explode(self::SEPARATOR, $value)) == 2 : true;
    }

    private function filterDate($query, $value, $column) {
        if ($this->range) {
            $dates = explode(self::SEPARATOR, $value);
            $dates = [
                Carbon::parse($dates[0])->format('Y-m-d 00:00:00'),
                Carbon::parse($dates[1])->format('Y-m-d 23:59:59'),
            ];
        }
        else {
            $dates = [
                Carbon::parse($value)->format('Y-m-d 00:00:00'),
                Carbon::parse($value)->format('Y-m-d 23:59:59'),
            ];
        }
        $query->whereBetween($column, $dates);
    }

    private function filterDateTime($query, $value, $column) {
        if ($this->range) {
            $dates = explode(self::SEPARATOR, $value);
            $dates = [
                Carbon::parse($dates[0])->format('Y-m-d H:i:s'),
                Carbon::parse($dates[1])->format('Y-m-d H:i:s'),
            ];
            $query->whereBetween($column, $dates);
        }
        else {
            $query->where($column, Carbon::parse($value)->format('Y-m-d H:i:s'));
        }
    }

    protected function filter($query, $value, $column) {
        if ($this->dateType == 'date') {
            $this->filterDate($query, $value, $column);
        }
        else if ($this->dateType == 'datetime') {
            $this->filterDateTime($query, $value, $column);
        }
    }

}