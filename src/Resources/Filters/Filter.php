<?php 

namespace MaxinTeam\Adele\Resources\Filters;

use Illuminate\Support\Str;

use MaxinTeam\Adele\Adele;
use MaxinTeam\Adele\Helpers\AdeleAccessable;

class Filter
{

    use AdeleAccessable;

    public string $type = 'filter';
    public string $label = '';
    public mixed $column = null;

    private $filterCustomCallback = null;
    protected $modifier = null;

    protected function __construct() {
        $this->initAccessor();
    }

    public static function make(string|callable $column, string $label = '') {
        $field = new static;
        $field->label = $label;
        if (is_callable($column)) {
            $field->bind($column);
        }
        else {
            $field->column = $column;
            if (!$field->label) {
                $field->label = $column;
            }
        }
        return $field;
    }

    final public function bind($callback) {
        $this->filterCustomCallback = $callback;
        return $this;
    }

    public function modifier($modifier) {
        $this->modifier = $modifier;
        return $this;
    }

    protected function filter($query, $value, $column) { 
        if (is_array($value)) {
            $query->whereIn($column, $value);
        }
        else {
            $query->where($column, $value);
        }
    }

    protected function checkValue($value) {
        return !is_null($value);
    }

    protected function boot() {}

    final public function get(string $name = '') {
        $this->boot();

        if ($this->type == 'filter') {
            $this->type = Str::camel(Str::after(class_basename($this), 'Filter'));
        }
        if (is_null($this->column)) {
            $this->column = '__custom__' . $name;
        }

        return $this;
    }

    final public function filtering($query, $value) {
        if ($this->modifier) {
            if (is_subclass_of($this->modifier, 'MaxinTeam\Adele\Resources\Filters\Modifiers\Modifier')) {
                $value = (new $this->modifier)->modify($value);
            }
            else if (is_callable($this->modifier)) {
                $value = ($this->modifier)($value, Adele::user());
            }
        }
        
        if ($this->checkValue($value)) {
            if (!is_null($this->filterCustomCallback)) {
                ($this->filterCustomCallback)($query, $value);
            }
            else {
                $this->filteringDefault($query, $value);
            }
        }
    }

    final protected function filteringDefault($query, $value) {
        $rels = explode('.', $this->column);
        if (count($rels) > 1) {
            $col = array_pop($rels);
            $has = implode('.', $rels);
            $query->whereHas($has, function($relQuery) use ($value, $col) {
                $this->filter($relQuery, $value, $col);
            });
        }
        else {
            $this->filter($query, $value, $this->column);
        }
    }

    public static function config($prop, $defaultValue = null) {
        return config('adele.resources.filters.' . $prop, $defaultValue);
    }

}