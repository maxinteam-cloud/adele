<?php 

namespace MaxinTeam\Adele\Resources\Filters;

use Illuminate\Support\Str;

class FilterSelect extends Filter
{
    public $options = [];
    public $multi = false;
    public $searchable = false;
    public $labelAll = '';

    private $selectModel = [
        'model' => null,
        'value' => 'id',
        'label' => 'id',
    ];
    
    private $optionsFilter = null;
    private $modifierOption = null;

    public function modifierOption($modifier) {
        $this->modifierOption = $modifier;
        return $this;
    }

    public function optionsFilter($callback) {
        $this->optionsFilter = $callback;
        return $this;
    }

    private function getOptionLabel($item) {
        $value = !is_string($this->selectModel['label']) ? ($this->selectModel['label'])($item) : $item->{$this->selectModel['label']};

        if ($this->modifierOption) {
            if (is_subclass_of($this->modifierOption, 'Adele\AdminPanel\Resources\Filters\Modifiers\Modifier')) {
                $value = (new $this->modifierOption)->modify($value);
            }
            else if (is_callable($this->modifierOption)) {
                $value = ($this->modifierOption)($value);
            }
        }
        return $value;
    }

    public function multi() {
        $this->multi = true;
        return $this;
    }

    public function searchable() {
        $this->searchable = true;
        return $this;
    }

    public function options($options) {
        $this->options = $options;
        return $this;
    }
    
    public function labelForAll($label) {
        $this->labelAll = $label;
        return $this;
    }

    public function fromModel($model = null, $labelColumn = 'id', $valueColumn = 'id') {
        $this->selectModel = [
            'model' => $model,
            'value' => $valueColumn,
            'label' => $labelColumn,
        ];
        return $this;
    }

    public function boot() {
        if (!$this->options) {
            if (is_null($this->selectModel['model'])) {
                $modelUse = '\App\Models\\'.Str::ucfirst(Str::camel(Str::beforeLast($this->column, '_id')));
                $this->selectModel['model'] = $modelUse;
            }

            $query = ($this->selectModel['model'])::query();
            if (!is_null($this->optionsFilter)) {
                ($this->optionsFilter)($query);
            }
            $options = $query->get();
            foreach ($options as $option) {
                $this->options[$option->{$this->selectModel['value']}] = $this->getOptionLabel($option);
            }
        }
        else {
            if (!is_null($this->optionsFilter)) {
                $this->options = ($this->optionsFilter)($this->options);
            }
        }

        if (!$this->labelAll) {
            $this->labelAll = self::config('select.labelAll', 'Все');
        }
    }

}