<?php 

namespace MaxinTeam\Adele\Resources\Filters;

class FilterNumber extends Filter
{

    public $min = PHP_INT_MIN;
    public $max = PHP_INT_MAX;
    public $step = 1;
    public $range = false;

    private $_operator = '=';

    public function larger() {
        $this->_operator = '>';
        return $this;
    }

    public function less() {
        $this->_operator = '<';
        return $this;
    }

    public function equally() {
        $this->_operator = '=';
        return $this;
    }

    public function largerOrEqually() {
        $this->_operator = '>=';
        return $this;
    }

    public function lessOrEqually() {
        $this->_operator = '<=';
        return $this;
    }

    public function notEqually() {
        $this->_operator = '<>';
        return $this;
    }

    public function operator($operator) {
        $this->_operator = $operator;
        return $this;
    }

    public function min($value) {
        $this->min = $value;
        return $this;
    }

    public function max($value) {
        $this->max = $value;
        return $this;
    }

    public function step($value) {
        $this->step = $value;
        return $this;
    }

    public function range() {
        $this->range = true;
        return $this;
    }

    protected function checkValue($value) {
        return $this->range ? is_array($value) && ((isset($value['from']) && !is_null($value['from'])) || (isset($value['to']) && !is_null($value['to']))) : !is_null($value);
    }

    protected function filter($query, $value, $column) {
        if (!$this->range) {
            $query->where($column, $this->_operator, $value);
        }
        else {
            if (isset($value['from']) && !is_null($value['from'])) {
                $query->where($column, '>=', $value['from']);
            }
            if (isset($value['to']) && !is_null($value['to'])) {
                $query->where($column, '<=', $value['to']);
            }
        }
    }

}