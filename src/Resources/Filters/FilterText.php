<?php 

namespace MaxinTeam\Adele\Resources\Filters;

class FilterText extends Filter
{

    private $likeStart = false;
    private $likeEnd = false;
    public $mask = null;

    public function likeStart() {
        $this->likeStart = true;
        return $this;
    }

    public function likeEnd() {
        $this->likeEnd = true;
        return $this;
    }

    public function like() {
        return $this->likeStart()->likeEnd();
    }

    public function mask($mask) {
        $this->mask = $mask;
        return $this;
    }

    public function modifier($modifier) {
        parent::modifier($modifier);
        // if (is_subclass_of($this->modifier, 'MaxinTeam\Adele\Resources\Filters\Modifiers\Modifier')) {
        //     $modifier = new $this->modifier;
        //     if (!$this->mask && $modifier->mask) {
        //         $this->mask = $modifier->mask;
        //     }
        // }
        return $this;
    }

    protected function filter($query, $value, $column) {
        $operand = $this->likeStart || $this->likeEnd ? 'like' : '=';
        $value = $operand == 'like' ? ($this->likeStart ? '%' : '').$value.($this->likeEnd ? '%' : '') : $value;
        
        $query->where($column, $operand, $value);
    }

}