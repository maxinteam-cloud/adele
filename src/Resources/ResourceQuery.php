<?php 

namespace MaxinTeam\Adele\Resources;

class ResourceQuery
{

    private $query = null;

    public function __construct($model) {   
        $this->query = $model::query();
    }

    public function setRels($fileds) {
        $with = collect([]);
        foreach ($fileds as $field) {
            if (is_string($field->column)) {
                $rels = $field->getPipelineColumn();
                if (count($rels) > 1) {
                    $pl = [];
                    unset($rels[count($rels) - 1]); // delete property from pipeline
                    foreach ($rels as $m) {
                        $pl[] = $m;
                        $with->push(implode('.', $pl));
                    }
                }
            }
        }

        foreach ($with->unique() as $withRel) {
            $this->query->with($withRel);
        }
    }

    public function setFilterValues($filters, $values) {
        foreach ($filters as $filter) {
            if (isset($values[$filter->column])) {
                $filter->filtering($this->query, $values[$filter->column]);
            }
        }
    }

    public function sorts($sorts) {
        foreach ($sorts as $column => $order) {
            $this->query->orderBy($column, $order);
        }
    }

    public function getQuery() {
        return $this->query;
    }

}