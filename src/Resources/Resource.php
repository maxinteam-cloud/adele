<?php

namespace MaxinTeam\Adele\Resources;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use MaxinTeam\Adele\Models\AdeleAdmin;
use MaxinTeam\Adele\Adele;

use MaxinTeam\Adele\Cards\Card;

use MaxinTeam\Adele\Resources\Actions\Standart\{ StandartAction, CreateAction, EditAction, DeleteAction };

abstract class Resource
{

    protected $model = null;
    protected $card = null;
    public $path = null;
    public $name = '';
    public $perPageOptions = [];
    public $perPageDefault = 0;

    protected array $fields = [];
    protected array $filters = [];

    protected $queryHandler = null;
    protected $sorts = [];
    public $filterDefaults = [];
    public $sortDefaultColumn = '';
    public $sortDefaultOrder = 'asc';

    protected array $actions = [];

    public function __construct() {
        $path = $this->getPath();
        
        if ($this->card) {
            $this->card = new $this->card;
            $this->card->setModel($this->model);
            $this->actions[Card::MODE_CREATE] = $this->registerStandartAction(new CreateAction($path, $this->card));
            $this->actions[Card::MODE_EDIT] = $this->registerStandartAction(new EditAction($path, $this->card));
        }

        $this->actions[Card::MODE_DELETE] = $this->registerStandartAction(new DeleteAction($path));

        if (!$this->model) {
            $this->setDefaultModel();
        }

        $this->boot(request(), Adele::user());

    }

    final protected function setDefaultModel() {
        $this->model = Str::before(class_basename($this), 'Resource');
    }

    final protected function registerStandartAction($action) : StandartAction {
        $action->setResource($this);
        return $action;
    }

    // Methods childs for use

    abstract protected function boot(Request $request, $admin);
    abstract protected function fields();
    abstract protected function filters();

    // ------

    // boot methods

    final protected function query(callable $queryHandler) : void {
        $this->queryHandler = $queryHandler;
    }

    final protected function filterDefault(array $filterDefaults) : void {
        $this->filterDefaults = $filterDefaults;
    }

    final protected function sort(string $column, string $order = 'asc') : void {
        $this->sorts[$column] = $order;
    }

    final protected function sortDefault(string $column, string $order = 'asc') : void {
        $this->sortDefaultColumn = $column;
        $this->sortDefaultOrder = $order;
    }

    final protected function getAction(string $mode) : StandartAction|null {
        $action = $this->actions[$mode] ?? null;
        return $action;
    }

    final protected function accessAction(string $mode, string|array|callable $access) : void {
        $action = $this->getAction($mode);
        if (is_null($action)) {
            throw new Exception("Action $mode not registered in resource");
        }

        if (is_callable($access)) {
            $action->access($access);
        }
        else {
            $action->onlyAccess($access);
        }
    }

    final protected function accessActions(array $accesses) : void {
        foreach ($accesses as $mode => $access) {
            $this->accessAction($mode, $access);
        }
    }

    final protected function notAccessAction(string $mode, string|array $access) : void {
        $action = $this->getAction($mode);
        $action->denyAccess($access);
    }

    // -----

    final public function getPath() {
        if (!$this->path) {
            $recourceName = class_basename($this);
            $this->path = Str::plural(Str::camel(Str::before($recourceName, 'Resource')));
        }
        return $this->path;
    }

    final public function getFields() : array {
        if (!$this->fields) {
            $fields = $this->fields(request());
            foreach ($fields as $fieldItem) {
                $field = $fieldItem->get();
                if ($field->isAccess()) {
                    $this->fields[] = $field;
                }
            }
        }

        return $this->fields;
    }

    final public function getFilters() : array {
        if (!$this->filters) {
            $filters = $this->filters(request());
            foreach ($filters as $k => $filterItem) {
                $filter = $filterItem->get($k);
                if ($filter->isAccess()) {
                    $this->filters[] = $filter;
                }
            }
        }

        return $this->filters;
    }

    final public function getQuery(array $filterValues = []) {
        $queryBuilder = new ResourceQuery($this->model);
        $queryBuilder->setRels($this->getFields());
        $queryBuilder->setFilterValues($this->getFilters(), $filterValues);
        $queryBuilder->sorts($this->sorts);
        
        $query = $queryBuilder->getQuery();

        if ($this->queryHandler) {
            ($this->queryHandler)($query);
        }

        return $query;
    }

    final public function getActions() : array {
        return $this->actions;
    }

    final public function getModel() {
        return $this->model;
    }

    final public function getCreateLink() : string|null {
        $createAction = $this->getAction(Card::MODE_CREATE);
        return $createAction && $createAction->isAccess() ? $createAction->path : null;
    }

}