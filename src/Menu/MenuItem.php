<?php 

namespace MaxinTeam\Adele\Menu;

use MaxinTeam\Adele\Router\Router;
use MaxinTeam\Adele\Router\Route;
use MaxinTeam\Adele\Helpers\AdeleAccessable;
use MaxinTeam\Adele\Resources\Resource;
use MaxinTeam\Adele\Cards\Card;

final class MenuItem
{

    use AdeleAccessable;

    public string|null $path = null;
    public string|null $fullPath = null;
    public string|null $label;
    public string $icon;

    public string $type = 'custom';
    public bool $isSystem = false;

    public Resource|null $resource = null;
    public Card|null $card = null;

    public function __construct() {
        $this->initAccessor();
    }

    public static function make(string|null $path, string|null $label, string|null $icon = null, callable|null $action = null) : self {
        $item = new self;
        $item->path = $path ? $path : null;
        $item->fullPath = $path ? Router::genFullPath($path) : null;
        $item->label = $label;
        $item->icon = $icon ? $icon : 'paw';
        // if ($action) {
        //     $item->registerAction($action);
        // }
        return $item;
    }

    public static function resource(string $resourceClass, $label = null, $icon = null) : self {
        $resource = new $resourceClass;
        if (!($resource instanceof Resource)) {
            throw new \Exception('Item menu ' . $resourceClass . ' is not Resource class'); 
        }
        $path = $resource->getPath();
        $item = self::make($path, $label ? $label : $resource::$title, $icon ? $icon : $resource::$iconMenu)->type('resource');
        $item->resource = $resource;
        
        $item->registerRoute();

        return $item;
    }

    public static function delimiter($label = null) : self {
        return self::make(null, $label)->type('delimiter');
    }
    
    private function type($type) : self {
        $this->type = $type;
        return $this;
    }

    private function registerRoute() : void {
        app(Router::class)->registerMenuItem($this);  
    }

    public function setAccessorIfNoSet(AdeleAccessor $accessor) : void {
        if (!$this->accessor()->isSet()) {
            $this->setAccessor($accessor);
        }
    }

}