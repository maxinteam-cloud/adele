<?php 

namespace MaxinTeam\Adele\Menu;

use MaxinTeam\Adele\Router\Router;

final class Menu {

    private Router $router;
    private array $items = [];

    public function __construct(Router $router) {
        $this->router = $router;
    }

    public function getRouter() : Router {
        return $this->router;
    }

    public function add(MenuItem|MenuGroup $item) : void {
        if ($item instanceof MenuGroup) {
            $this->items = array_merge($this->items, $item->getItems());
        }
        else {
            $this->items[] = $item;
        }
    }

    private function getAccesableItems() : array {
        $items = $this->items;
        foreach ($items as $k => $item) {
            if (!$item->isAccess()) {
                unset($items[$k]);
            }
        }
        usort($items, function($a, $b) {
            return $a->isSystem == $b->isSystem ? 0 : ($a->isSystem ? 1 : -1);
        });
        return $items;
    }

    public function getItemsForView() : array {
        $items = [];
        foreach ($this->getAccesableItems() as $item) {
            $items[] = (object)[
                'path' => $item->fullPath,
                'icon' => $item->icon,
                'label' => $item->label,
                'type' => $item->type,
            ];
        }
        return $items;
    }

}