<?php 

namespace MaxinTeam\Adele\Menu;

use MaxinTeam\Adele\Helpers\AdeleAccessable;

final class MenuGroup {

    use AdeleAccessable;

    private array $items = [];
    private string $order = '0';
    private bool $isSystem = false;

    private function __construct() {
        $this->initAccessor();
    }

    private function system() : self {
        $this->isSystem = true;
        return $this;
    }

    private function items(array $items) : self {
        $this->items = $items;
        return $this;
    }

    public function getItems() : array {
        if ($this->accessor->isSet()) {
            foreach ($this->items as $item) {
                $item->setAccessorIfNoSet(clone $this->accessor);
            }
        }
        if ($this->isSystem) {
            foreach ($this->items as $item) {
                $item->isSystem = true;
            }
        }

        return $this->items;
    }

    public static function __callStatic($method, $parameters) {
        $self = new self;
        return $self->$method(...$parameters);
    }

    public function __call($method, $parameters) {
        if (method_exists($this, $method)) {
            return call_user_func_array([$this, $method], $parameters);
        }

        throw new \BadMethodCallException("Adele MenuGroup method [$method] does not exist.");
    }
    
}