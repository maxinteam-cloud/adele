<?php 

namespace MaxinTeam\Adele\Router;

use Illuminate\Support\Facades\Route as BaseRoute;
use Illuminate\Support\Str;
use Illuminate\Session\Middleware\StartSession;

use MaxinTeam\Adele\Menu\MenuItem;

use MaxinTeam\Adele\Adele;

use MaxinTeam\Adele\Resources\Resource;
use MaxinTeam\Adele\Helpers\AdeleAccessor;

use MaxinTeam\Adele\Http\Controllers\Controller;
use MaxinTeam\Adele\Http\Controllers\Auth\LoginController;
use MaxinTeam\Adele\Http\Controllers\ResourceController;
// use MaxinTeam\Adele\Http\Controllers\CardController;

final class Router
{

    private string $path = ''; 
    private string|null $domain;
    private string $prefixName;

    private array $routes = [];

    public function __construct() {
        $this->path = $this->getPath();
        $this->domain = config('adele.router.domain', null);
        $this->prefixName = config('adele.router.prefixName', 'adele');
    }

    private function getPath() {
        if (!$this->path) {
            $path = trim(config('adele.router.path', 'admin'), '/');
            $this->path = $path ? "/$path/" : '/';
        }
        return $this->path;
    }

    private function routeGroup() {
        $path = $this->getPath();
        $routeGroup = $this->domain ? BaseRoute::domain($this->domain)->prefix($path) : BaseRoute::prefix($path);
        $routeGroup->name($this->prefixName . '.');
        return $routeGroup;
    }

    private function registerRoutes(array $routes = [], array $middleware = ['adele']) {
        $routeGroup = $this->routeGroup();
        if ($middleware) {
            $routeGroup->middleware($middleware);
        }
        $routeGroup->group(function() use ($routes) {
            foreach ($routes as $route) {
                $route->register();
                $this->routes[$route->name] = $route;
            }
        });
    }

    private function registerResource(string $path, string $title, Resource $resource, AdeleAccessor $accessor = null) : void {
        $this->registerRoutes([
            Route::get($path, [ResourceController::class, 'index'], $path, $title)->entities([$resource])->setAccessor($accessor),
            Route::post($path, [ResourceController::class, 'items'], $path . '.items', $title)->entities([$resource])->setAccessor($accessor),
        ]);

        $actions = $resource->getActions();
        $actionsRoutes = [];
        foreach ($actions as $action) {
            foreach ($action->getHandlers() as $method => $actionHandler) {
                $actionsRoutes[] = Route::{$method}($action->path, $actionHandler, $path . '.actions.' . $action->name . '--' . $method, $title)->entities([$resource, $action]);
            }
        }
        $this->registerRoutes($actionsRoutes);

        // $card = $resource->getCard();
        // if ($card) {
        //     $this->registerRoutes([
        //         Route::get($path . '/create', [CardController::class, 'create'], $path . '.create', $title . ' - создание', $resource, $card)->type('resourceCard'),
        //         Route::post($path . '/create', [CardController::class, 'createSave'], $path . '.createSave', $title . ' - создание', $resource, $card)->type('resourceCard'),
        //         Route::get($path . '/{id}', [CardController::class, 'edit'], $path . '.edit', $title . ' - редактирование', $resource, $card)->type('resourceCard'),
        //         Route::post($path . '/{id}', [CardController::class, 'editSave'], $path . '.editSave', $title . ' - редактирование', $resource, $card)->type('resourceCard'),
        //         // Route::get($path . '/{id}', function() {}, $path . '.show', $item->label . ' - просмотр', $item->resource, $card),
        //         Route::delete($path . '/{id}', [CardController::class, 'delete'], $path . '.delete', $title . ' - удаление', $resource, $card)->type('resourceCard'),
        //     ]);
        // }
    }
    

    public function registerMenuItem(MenuItem $item) {
        $path = $item->path;
        if ($item->type == 'resource') {
            if (!isset($this->routes[$path])) {
                $this->registerResource($path, $item->label, $item->resource, $item->accessor());
            } 
        }
    }

    public function getRoute(string $name) {
        $endName = Str::replaceFirst($this->prefixName . '.', '', $name);
        return $this->routes[$endName] ?? null;
    }

    public static function genNameRoute(string $name) {
        $router = app(self::class);
        return $router->prefixName . '.' . $name;
    }

    public static function getRouteOrFail(string $name) {
        $router = app(self::class);
        $route = $router->getRoute($name);
        if (!$route) {
            abort(404);
        }
        return $route;
    }

    public static function genFullPath(string $path) {
        $router = app(self::class);
        return $router->getPath() . $path;
    }

    public function registerAuthRoutes() {
        $this->registerRoutes([
            Route::get('login', [LoginController::class, 'index'], 'login'),
            Route::post('login', [LoginController::class, 'check'], 'login.check'),
            Route::get('logout', [LoginController::class, 'logout'], 'logout'),
        ], [StartSession::class]);

        app(Adele::class)->getViewData()->routeAlias('login', $this->genNameRoute('login'));
        app(Adele::class)->getViewData()->routeAlias('logout', $this->genNameRoute('logout'));
    }

    public function registerMainRoutes() {
        $this->registerRoutes([
            Route::get('/', [Controller::class, 'home'], 'home')
        ]);
    }

}