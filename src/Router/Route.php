<?php 

namespace MaxinTeam\Adele\Router;

use Illuminate\Support\Facades\Route as BaseRoute;

use MaxinTeam\Adele\Resources\Resource;
use MaxinTeam\Adele\Resources\Actions\Action;
use MaxinTeam\Adele\Cards\Card;

use MaxinTeam\Adele\Helpers\AdeleAccessable;

final class Route
{

    use AdeleAccessable;

    public string $path = '';
    public string $name = '';
    public $action = null;
    public string $method = '';
    public array $entities = [];
    private bool $registered = false;

    public function __construct(string $path, callable|array $action, string $name) {
        $this->path = $path;
        $this->action = $action;
        $this->name = $name;
    }

    public function entities(array $entities) : self {
        $this->entities = $entities;
        return $this;
    }

    public function register() : void {
        if (!$this->registered) {
            BaseRoute::{$this->method}($this->path, $this->action)->name($this->name);
            $this->registered = true;
        }
    }

    private function getEntity() {
        return $this->entities[0];
    }

    public function getResource() {
        foreach ($this->entities as $entity) {
            if ($entity instanceof Resource) {
                return $entity;
            }
        }

        return null;
    }
    
    public function getAction() {
        foreach ($this->entities as $entity) {
            if ($entity instanceof Action) {
                return $entity;
            }
        }

        return null;
    }

    public static function __callStatic(string $method, array $parameters) : self {
        if (!in_array($method, ['get', 'post', 'delete'])) {
            throw new \Exception("Route method [$method] not passed");
        }
        
        $route = new self(...$parameters);
        $route->method = $method;
        return $route;
    }

}