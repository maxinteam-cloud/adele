<?php 

namespace MaxinTeam\Adele\Cards\Fields;

class Text extends Field
{

    public $inputType = 'text';
    public $big = false;
    public mixed $value = '';

    public function big() : static {
        $this->big = true;
        return $this;
    }

    public function inputType($type) : static {
        $this->inputType = $type;
        return $this;
    }

}