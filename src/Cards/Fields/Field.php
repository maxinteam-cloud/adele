<?php 

namespace MaxinTeam\Adele\Cards\Fields;

use Illuminate\Support\Str;

use MaxinTeam\Adele\Helpers\AdeleAccessable;
use MaxinTeam\Adele\Cards\Card;

abstract class Field
{

    use AdeleAccessable;

    public string $type = 'field';
    public string $column = '';
    public string $label = '';
    public mixed $value = '';
    protected array $mods = [];
    protected string|array $validation = [];
    protected $modifier;
    protected $modifierBeforeSave;

    protected function __construct() {
        $this->initAccessor();
    }

    public static function make(string $column, string $label = '') : static {
        $field = new static;
        $field->column = $column;
        $field->label = $label ?? $column;
        return $field;
    }

    public function boot() : void {
        //
    }

    final public function get() : static {
        $this->boot();

        if ($this->type == 'field') {
            $this->type = Str::camel(class_basename($this));
        }

        return $this;
    }

    final public function forCreate() : static {
        $this->mods = [Card::MODE_CREATE];
        return $this;
    }

    final public function forEdit() : static {
        $this->mods = [Card::MODE_EDIT];
        return $this;
    }

    final public function validate(array|string $rules) : static {
        $this->validation = $rules;
        return $this;
    }

    final public function modifier($modifier) : static {
        $this->modifier = $modifier;
        return $this;
    }

    final public function modifierBeforeSave($modifier) : static {
        $this->modifierBeforeSave = $modifier;
        return $this;
    }

    protected function getValueFromObject($object) {
        return $object->{$this->column};
    }

    final protected function modifyValue($modifier, $object, $originaObject = null) {
        if ($modifier) {
            if (is_subclass_of($modifier, Modifier::class)) {
                $modifierClass = $modifier;
                $modifier = new $modifierClass($object, $originaObject);
                $this->value = $modifier->modify($this->value);
            }
            else if (is_callable($this->modifier)) {
                $this->value = ($this->modifier)($this->value, $object, $originaObject);
            }
        }
    }

    final public function setValueFromObject($object, $originalObject = null) {
        $this->value = $this->getValueFromObject($object);
        $this->modifyValue($this->modifier, $object, $originalObject);
    }

    final public function setValue($value, $object, $originalObject) {
        $this->value = $value;
        $this->modifyValue($this->modifierBeforeSave, $object, $originalObject);
    }

    public static function config($prop, $defaultValue = null) {
        return config('adele.cards.fields.' . $prop, $defaultValue);
    }

}