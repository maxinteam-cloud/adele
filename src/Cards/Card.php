<?php

namespace MaxinTeam\Adele\Cards;

use MaxinTeam\Adele\Http\Controllers\CardController;
use MaxinTeam\Adele\Resources\Resource;

abstract class Card
{

    const MODE_CREATE = 'create';
    const MODE_EDIT = 'edit';
    const MODE_DELETE = 'delete';

    protected array $fields = [];
    protected array $params = [];
    protected $model;
    protected mixed $object = null;
    protected mixed $originalObject = null;
    protected string $mode;
    protected bool $prepared = false;

    protected function boot() {}
    abstract protected function fields();

    final public function setParams(array $params = []) : void {
        $this->params = $params;
    }

    final protected function param(string $name, mixed $defaultValue = null) : mixed {
        return $this->params[$name] ?? $defaultValue;
    }

    final protected function allParams() : array {
        return $this->params;
    }

    final protected function setMode(string $mode) : void {
        $this->mode = $mode;
    }

    final public function setModeCreate() : void {
        $this->setMode(self::MODE_CREATE);
    }

    final public function setModeEdit() : void {
        $this->setMode(self::MODE_EDIT);
    }

    final public function isMode(string $mode) : bool {
        return $this->mode == $mode;
    }

    final public function isModeCreate() : bool {
        return $this->isMode(self::MODE_CREATE);
    }

    final public function isModeEdit() : bool {
        return $this->isMode(self::MODE_EDIT);
    }

    final public function getAccesableFields() : array {
        $this->prepare();
        if (!$this->fields) {
            $fields = $this->fields() ?? [];
            foreach ($fields as $fieldItem) {
                $field = $fieldItem->get();
                if ($field->isAccess()) {
                    if ($this->originalObject) {
                        $field->setValueFromObject($this->originalObject);
                    }
                    $this->fields[] = $field;
                }
            }
        }
        return $this->fields;
    }

    final protected function prepare() : void {
        if (!$this->prepared) {
            $this->boot();
            if ($this->isModeEdit()) {
                $this->beforeEdit($this->object);
            }
            $this->prepared = true;
        }
    }

    final protected function setObject(mixed $object) {
        $this->object = $object;
    }

    final protected function setOriginalObject(mixed $object) {
        $this->originalObject = $object;
    }

    final public function setModel(string $model) : void {
        $this->model = $model;
    }

    final public function setOriginalObjectById(int $id) : void {
        $originalObject = ($this->model)::findOrFail($id);
        $this->setOriginalObject($originalObject);
    }

    final public function getHandlers() {
        return [
            'get' => [CardController::class, $this->isModeCreate() ? 'create' : 'edit'],
            'post' => [CardController::class, $this->isModeCreate() ? 'store' : 'update'],
        ];
    }

    final public function validate() {
        return [];
    }

    final public function fillObject(array $data) {
        $this->object = $this->originalObject ? clone($this->originalObject) : (new $this->model);
        $this->object->fill($data);
    }

    final public function save() {
        if ($this->isModeCreate()) {
            $this->beforeCreateSave($this->object);
            $this->object->save();
            $this->afterCreateSave($this->object);
        }
        else if ($this->isModeEdit()) {
            $this->beforeEditSave($this->object, $this->originalObject);
            $this->object->save();
            $this->afterEditSave($this->object, $this->originalObject);
        }
    }

    // hooks

    protected function beforeCreateSave($object) : void {}
    protected function afterCreateSave($object) : void {}
    protected function beforeEdit($object) : void {}
    protected function beforeEditSave($object, $originalObject) : void {}
    protected function afterEditSave($object, $originalObject) : void {}

}