<?php 

namespace MaxinTeam\Adele\Helpers;

use MaxinTeam\Adele\Helpers\AdeleAccessor;

trait AdeleAccessable
{

    private AdeleAccessor|null $accessor = null;

    public function initAccessor() {
        $this->accessor = new AdeleAccessor;
    }

    public function accessor() {
        if (is_null($this->accessor)) {
            $this->accessor = new AdeleAccessor;
        }
        return $this->accessor;
    }

    public function setAccessor(AdeleAccessor $accessor) {
        $this->accessor = $accessor;
        return $this;
    }

    public function accessOnly(array|string $groups) : self {
        $this->accessor()->only($groups);
        return $this;
    }

    public function accessDeny(array|string $groups) : self {
        $this->accessor()->deny($groups);
        return $this;
    }

    public function access(callable $cb) : self {
        $this->accessor()->setCallback($cb);
        return $this;
    }

    public function isAccess() : bool {
        return $this->accessor()->isAccess();
    }

}