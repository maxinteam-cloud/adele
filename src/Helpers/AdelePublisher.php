<?php

namespace MaxinTeam\Adele\Helpers;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AdelePublisher
{

    public static function adelePath() {
        $adelePath =  app_path('Adele') . '/';

        // Create admin panel directory if not exists
        if (!file_exists($adelePath) || !is_dir($adelePath)) {
            mkdir($adelePath);
        }

        return $adelePath;
    }

    public static function clearPathWay(string $pathway) : string {
        return Str::replace('\\', '/', $pathway);
    }

    public static function publishDir(string $dir) : bool {
        try {
            $adminPanelPath = self::adelePath();
            $fullWay = [];
            $dirs = explode('/', $dir);
            foreach ($dirs as $dir) {
                $fullWay[] = $dir;
                $appPathDir = $adminPanelPath . implode('/', $fullWay);
                if (!file_exists($appPathDir) || !is_dir($appPathDir)) {
                    mkdir($appPathDir);
                }
            }

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    public static function publishDirClearFile(string $file) {
        $fileway = self::clearPathWay($file);
        $dirs = explode('/', $fileway);
        unset($dirs[count($dirs) - 1]);
        self::publishDir(implode('/', $dirs));
    }

    public static function publishFile(string $file, string $content) : bool {
        try {
            $adelePath = self::adelePath();
            self::publishDirClearFile($file);
            
            $appPathFile = $adelePath . '/' . self::clearPathWay($file);
            if (file_exists($appPathFile)) {
                echo "$file is exists!" . PHP_EOL;
                return false;
            }

            file_put_contents($appPathFile, $content);

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    public static function renderTemplate(string $template, array $data = []) : string {
        $prepareData = [
            'sourcePath' => 'Adele',
        ];
        return view('adele::' . $template, $data + $prepareData)->render();
    }

    public static function publishesFromTemplate(string $file, string $template, array $data = []) : bool {
        $content = self::renderTemplate($template, $data);
        return self::publishFile($file, $content);
    }

    public static function prepareClassway(string $classway) : string {
        return Str::replace('/', '\\', $classway);
    }

    public static function classNamespace(string $classway, string $defaultPrefix = '') : string {
        $classwayPrepared = self::prepareClassway($classway);
        return Str::contains($classwayPrepared, '\\') ? trim($classwayPrepared, '\\') : $defaultPrefix . $classwayPrepared;
    }

    public static function className(string $classway) : string {
        $classwayPrepared = self::prepareClassway($classway);
        if (Str::contains($classwayPrepared, '\\')) {
            $ns = explode('\\', $classwayPrepared);
            return end($ns);
        }

        return $classwayPrepared;
    }

}