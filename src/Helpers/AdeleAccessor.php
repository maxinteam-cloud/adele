<?php 

namespace MaxinTeam\Adele\Helpers;

use MaxinTeam\Adele\Adele;

use Illuminate\Support\Arr;

final class AdeleAccessor
{

    private array $only = [];
    private array $deny = [];
    private mixed $callback = null;

    private bool $isSet = false;

    private bool $access = true;

    public function only(array|string $groups) : void {
        $this->only = Arr::wrap($groups);
        $this->isSet = true;
    }

    public function deny(array|string $groups) : void {
        $this->deny = Arr::wrap($groups);
        $this->isSet = true;
    }

    public function setCallback(callable $cb) : void {
        $this->callback = $cb;
        $this->isSet = true;
    }

    public function setAccess(bool $access) : void {
        $this->access = $access;
        $this->isSet = true;
    }

    public function isAccess() : bool {
        $user = Adele::user();
        if (!$user->isSuperUser()) {
            if ($user) {
                if ($this->callback) {
                    $this->access = ($this->callback)(request(), $user);
                }
                else if ($this->deny) {
                    $this->access = !$user->hasRole($this->deny);
                }
                else if ($this->only) {
                    $this->access = $user->hasRole($this->only);
                }
            }
            else {
                $this->access = false;
            }
        }
        else {
            $this->access = true;
        }

        return $this->access;
    }

    public function isSet() : bool {
        return $this->isSet;
    }

    public function getCallback() : mixed {
        return $this->callback;
    }

}