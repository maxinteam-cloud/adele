<?php 

namespace MaxinTeam\Adele\Modifiers;

use MaxinTeam\Adele\Adele;
use MaxinTeam\Adele\Models\AdeleAdmin;

abstract class Modifier
{

    public $mask = '';
    protected AdeleAdmin $admin;
    protected $object;
    protected $originalObject;

    public function __construct($object = null, $originalObject = null) {
        $this->admin = Adele::user();
        $this->object = $object;
        $this->originalObject = $originalObject;
    }

    abstract public function modify($value);

}