<?php 

namespace MaxinTeam\Adele;

use MaxinTeam\Adele\Models\AdeleAdmin;
use Illuminate\Support\Facades\Hash;

final class AdeleAuthGuard
{

    public $user = null;

    const TOKEN_NAME = 'adele_token';
    const LOGIN_REDIRECT_URI_NAME = 'adele_login_redirect';

    public static function auth() {
        $auth = app(self::class);
        if (!$auth->user) {
            $token = session()->get(self::TOKEN_NAME);
            if ($token) {
                $auth->user = AdeleAdmin::where('token', $token)->first();
                if ($auth->user) {
                    app(Adele::class)->getViewData()->setUser($auth->user);
                }
            }
        }
        return $auth->user;
    }

    public static function getUser() {
        $auth = app(self::class);
        return $auth->user; 
    }

    public function setAuth() {
        $token = md5('__ADELE_ADMIN_TOKEN__'.time().'-'.$this->user->id);
        session()->put(self::TOKEN_NAME, $token);
        $this->user->token = $token;
        $this->user->save();
    }

    public static function login($login, $pass) {
        $admin = AdeleAdmin::where('login', $login)->first();
        if ($admin && Hash::check($pass, $admin->password)) {
            $auth = app(self::class);
            $auth->user = $admin;
            $auth->setAuth();
            return true;
        }

        return false;
    }

    public static function logout() {
        session()->forget(self::TOKEN_NAME);
        session()->flush();
    }

    public static function setRedirectUrlAfterAuth($url) {
        session()->put(self::LOGIN_REDIRECT_URI_NAME, $url);
    }

    public static function pullRedirectUrlAfterAuth() {
        return session()->pull(self::LOGIN_REDIRECT_URI_NAME, null);
    }

}