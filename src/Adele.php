<?php

namespace MaxinTeam\Adele;

use MaxinTeam\Adele\Menu\Menu;

final class Adele
{

    private Menu $menu;
    private AdeleView $view;

    public function __construct(Menu $menu, AdeleView $view) {
        $this->menu = $menu;
        $this->view = $view;
    }

    public function getMenu() {
        return $this->menu;
    }

    public function getRouter() {
        return $this->menu->getRouter();
    }

    public function getView() {
        return $this->view;
    }

    public function getViewData() {
        return $this->view->getData();
    }

    public static function menu(array $items) {
        foreach ($items as $item) {
            app(self::class)->getMenu()->add($item);
        }
    }

    public static function user() {
        $user = AdeleAuthGuard::getUser();
        return $user;
    }

}