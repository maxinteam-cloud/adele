<?php

namespace MaxinTeam\Adele;

use MaxinTeam\Adele\Models\AdeleAdmin;

use View;

final class AdeleViewData
{

    public AdeleAdmin $admin;
    public array $nav = [];
    public string $title = 'Админка';
    public array $resourceSettings = [
        'perPage' => [
            'default' => 10,
            'options' => [10, 25, 50, 100],
        ]
    ];

    private array $routeAliases = [];

    public function __construct() {
        $this->resourceSettings['perPage']['default'] = config('adele.resource.perPage.default', $this->resourceSettings['perPage']['default']);
        $this->resourceSettings['perPage']['options'] = config('adele.resource.perPage.options', $this->resourceSettings['perPage']['options']);
    }

    public function setUser(AdeleAdmin $admin) : void {
        $this->admin = $admin;
    }

    public function setNav(array $items) : void {
        $this->nav = $items;
    }

    public function routeAlias(string $alias, string $name) : void {
        $this->routeAliases[$alias] = $name;
    }

    public function getPathRouteByAlias(string $alias) : string {
        return route($this->routeAliases[$alias]);
    }

    public function getPublicData() {
        return [
            'admin' => $this->admin ? $this->admin->publicData : null,
        ];
    }

    public function shareGlobal() {
        View::share('__adele', $this);
    }

}