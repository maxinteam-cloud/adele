<?php

namespace MaxinTeam\Adele;

use MaxinTeam\Adele\Models\AdeleAdmin;

final class AdeleView
{

    const VIEW_DIR = 'adele';

    private AdeleViewData $data;

    public function __construct(AdeleViewData $data) {
        $this->data = $data;        
    }

    public function getData() {
        return $this->data;
    }

    public function view(string $template, array $data = []) {
        return view(self::VIEW_DIR . '::' . $template, $data);
    }

}