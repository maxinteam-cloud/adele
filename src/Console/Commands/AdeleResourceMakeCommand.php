<?php

namespace MaxinTeam\Adele\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Str;
use MaxinTeam\Adele\Helpers\AdelePublisher;

class AdeleResourceMakeCommand extends Command
{

    protected $signature = 'adele:resource {name} {?--model=} {?--card=}';
    protected $description = 'Adele make new resource';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle() : void
    {
        $name = $this->argument('name');
        $model = $this->option('model');
        if (!$model) {
            $model = Str::remove('Resource', $name);
        }

        // if (!class_exists('App\Adele\Cards\\'.$card)) {
        //     $publishedCard = $this->publishesFromTemplate('Cards', $name.'.php', 'entities.Card', [
        //         'className' => $name,
        //     ]);
    
        //     if ($publishedCard) {
        //         $this->info("Card \"$name\" created");
        //     }
        // }
        
        $published = AdelePublisher::publishesFromTemplate("Resources/$name.php", 'entities.Resource', [
            'className' => $name,
            'modelName' => AdelePublisher::className($model),
            'modelUse' => AdelePublisher::classNamespace($model, 'App\Models\\'),
            'cardName' => null,
            'cardUse' => null,
        ]);
        if ($published) {
            $this->info("Resource \"$name\" created");
        }
        else {
            $this->error('Something went wrong!');
        }

    }
}
