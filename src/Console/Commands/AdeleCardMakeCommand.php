<?php

namespace MaxinTeam\Adele\Console\Commands;

use Illuminate\Console\Command;

use MaxinTeam\Adele\Helpers\AdelePublisher;

class AdeleCardMakeCommand extends Command
{

    protected $signature = 'adele:card {name}';
    protected $description = 'Adele make new card';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle() : void
    {
        $name = $this->argument('name');
        
        $published = AdelePublisher::publishesFromTemplate("Cards/$name.php", 'entities.Card', [
            'className' => $name,
        ]);
        if ($published) {
            $this->info("Card \"$name\" created");
        }
        else {
            $this->error('Something went wrong!');
        }

    }
}
