<?php

namespace MaxinTeam\Adele\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Str;
use MaxinTeam\Adele\Helpers\AdelePublisher;

class AdeleModifierMakeCommand extends Command
{

    protected $signature = 'adele:modifier {name}';
    protected $description = 'Adele make new modifier';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle() : void
    {
        $name = $this->argument('name');
        
        $published = AdelePublisher::publishesFromTemplate("Modifiers/$name.php", 'entities.Modifier', [
            'className' => $name,
        ]);
        if ($published) {
            $this->info("Modifier \"$name\" created");
        }
        else {
            $this->error('Something went wrong!');
        }

    }
}
