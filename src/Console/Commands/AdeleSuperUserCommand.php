<?php

namespace MaxinTeam\Adele\Console\Commands;

use Illuminate\Console\Command;

use MaxinTeam\Adele\Models\AdeleAdminRole;
use MaxinTeam\Adele\Models\AdeleAdmin;

use Hash;

class AdeleSuperUserCommand extends Command
{

    protected $signature = 'adele:superuser';
    protected $description = 'Adele create or set new password super user';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle() : void
    {

        $password = 'admin';
        if ($this->confirm('Do you wish set you password for admin user?')) {
            $password = $this->secret('You password for admin user');
        }

        $role = AdeleAdminRole::where('code', AdeleAdminRole::SUPERUSER_CODE)->first();
        if (!$role) {
            $role = AdeleAdminRole::create([
                'name' => 'Super user',
                'code' => AdeleAdminRole::SUPERUSER_CODE,
            ]);
            $this->info('Super user role create!');
        }

        $admin = AdeleAdmin::where('login', 'admin')->first();
        if (!$admin) {
            $admin = AdeleAdmin::create([
                'login' => 'admin',
                'password' => Hash::make($password),
                'name' => 'Admin'
            ]);
            $this->info('Admin create!');
        }
        else {
            $admin->password = Hash::make($password);
            $admin->save();
            $this->info('Admin set new password!');
        }

        $admin->roles()->sync($role);

    }
}
