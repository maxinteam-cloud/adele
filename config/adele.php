<?php 

return [

    'router' => [
        'domain' => env('ADELE_DOMAIN', null),
        'path' => env('ADELE_PATH', 'admin'),
        'prefixName' => 'adele',
    ],

    'access' => [
        'resource' => [
            'only' => [],
            'deny' => [],
        ],
        'card' => [
            'create' => [
                'only' => [],
                'deny' => [],
            ],
            'edit' => [
                'only' => [],
                'deny' => [],
            ],
            'delete' => [
                'only' => [],
                'deny' => [],
            ],
        ]
    ],

    // 'resource' => [
    //     'perPage' => [
    //         'default' => 10,
    //         'options' => [10, 25, 50, 100],
    //     ],
    //     'fields' => [
    //         'date' => [
    //             'formatDate' => 'd.m.Y',
    //             'formatDatetime' => 'd.m.Y H:i',
    //             'formatTime' => 'H:i:s',
    //         ],
    //     ],
    //     'accessActions' => [
    //         'create' => 'manager;author', // Резрешить только роялм manager и author
    //         'edit' => true, // Разрешить всем
    //         'delete' => '!viewer', // Запретить только роли viewer
    //     ]
    // ]

    'resources' => [
        'perPage' => [
            'default' => 10,
            'options' => [10, 25, 50, 100],
        ],
        'fields' => [
            'id' => [
                'column' => 'id',
                'label' => 'ID'
            ],
            'date' => [
                'format' => [
                    'date' => 'd.m.Y',
                    'datetime' => 'd.m.Y H:i',
                    'time' => 'H:i:s',
                ],
            ],
            'boolean' => [
                'true' => 'Да',
                'false' => 'Нет'
            ],
        ],
        'filters' => [
            'select' => [
                'labelAll' => 'Все',
            ],
        ],
    ],

];